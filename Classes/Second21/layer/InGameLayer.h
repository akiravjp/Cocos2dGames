#ifndef _IN_GAME_LAYER_H_
#define _IN_GAME_LAYER_H_

#include "cocos2d.h"
#include "cocos/editor-support/cocostudio/CocoStudio.h"
#include "cocos/ui/CocosGUI.h"

#include "../Object/Player.h"
#include "../Object/Ball.h"

class InGameLayer : cocos2d::Layer
{
public:

	InGameLayer(void);
	~InGameLayer(void);

public:

	static cocos2d::Scene* createScene();

public:

	virtual bool init();
	virtual bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event); 
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event); 
	virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event); 
	virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	CREATE_FUNC(InGameLayer);

protected:

	/* update定时器回调 */
	virtual void update(float dt);

private:

	/* 注册触屏事件 */
	void addTouchEventListener();

	/* 自定义定时器回调 */
	void gameSetp(float dt);

	/* 移动所有小球 */
	void moveAllBalls();

	/* 碰撞 */
	void hitAllBalls();

	/* 游戏结呸 */
	void gameOver();

	/* 释放内存 */
	void distroy();
	
	void timeCount(float dt);

private:

	cocos2d::Sprite* background;

	Player* mPlayer;

	cocos2d::Point curTouchPos;

	cocos2d::Vector<Ball*> ballList; 

	const float m_totalTime;
	float m_costTime;
};

#endif	//_IN_GAME_LAYER_H_


