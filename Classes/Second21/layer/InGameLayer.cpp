#include "InGameLayer.h"
#include "Base/VisibleRect.h"

using namespace cocos2d;
using namespace ui;
using namespace cocostudio;


InGameLayer::InGameLayer(void) :	background(nullptr),
									mPlayer(nullptr),
									curTouchPos(), 
									m_totalTime(21), 
									m_costTime(0)
{
}


InGameLayer::~InGameLayer(void)
{
}

Scene* InGameLayer::createScene()
{
	auto sc		=	Scene::create();
	auto ly		=	InGameLayer::create();
	sc->addChild(ly);

	return sc;
}

bool InGameLayer::init()
{
	Layer::init();

	Size winSize	=	Director::getInstance()->getVisibleSize();

	background		=	Sprite::create("Second21/gameFace.png");
	this->addChild(background);
	background->setAnchorPoint(Point::ZERO);

	LoadingBar* loadingBar = LoadingBar::create("Second21/progressBar.png");
	this->addChild(loadingBar);	

	loadingBar->setName("loadingBar");
	loadingBar->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
	loadingBar->setPosition(Point(410, 610));
    loadingBar->setPercent(100);

	auto labelCostTime = LabelAtlas::create("0123456789x,", "Second21/num_score.png", 31, 41, '0');
	this->addChild(labelCostTime);

	
	labelCostTime->setName("labelCostTime");
	labelCostTime->setAnchorPoint(Point::ANCHOR_TOP_LEFT);	
	labelCostTime->setPosition(133, 610);

	mPlayer			=	Player::create();
	this->addChild(mPlayer);
	mPlayer->setPosition(winSize.width / 2,winSize.height / 2);

	curTouchPos		=	mPlayer->getPosition();

	//1.开启定时器

	this->scheduleUpdate();
	this->schedule(SEL_SCHEDULE(&InGameLayer::gameSetp),0.02f);

	//2.定时器的回调函数原型

	//创建返弹小球
	Ball* mBall	=	nullptr;
	for (int i = 0; i < 5; i++)
	{
		mBall	=	Ball::create();
		mBall->setPosition(CCRANDOM_0_1() * winSize.width,CCRANDOM_0_1() * winSize.height);
		this->addChild(mBall);
		ballList.pushBack(mBall);
	}

	addTouchEventListener();

	return true;
}

void InGameLayer::update(float dt)
{

}

void InGameLayer::gameSetp(float dt)
{
	mPlayer->move(curTouchPos);

	moveAllBalls();

	hitAllBalls();

	timeCount(dt);
}

void InGameLayer::timeCount(float dt)
{
	m_costTime += dt;

	LoadingBar *loadingBar = dynamic_cast<LoadingBar*>(this->getChildByName("loadingBar"));
	float curPercent = (m_totalTime - m_costTime) * 100 / m_totalTime;
	loadingBar->setPercent(curPercent);

	char chCostTime[32];
	snprintf(chCostTime, sizeof(chCostTime), "%2d", (int)m_costTime);
	auto labelCostTime = dynamic_cast<LabelAtlas*>(this->getChildByName("labelCostTime"));
	labelCostTime->setString(chCostTime);

	if (curPercent < 0)
	{
		gameOver();
	}	
	return ;
}

void InGameLayer::moveAllBalls()
{
	for(auto obj : ballList)
	{
		obj->move();
	}
}

void InGameLayer::hitAllBalls()
{
	for(auto obj : ballList)
	{
		bool isHited = mPlayer->hitTest(obj);
		if(isHited)
		{
			gameOver();
			break;
		}
	}
}

void InGameLayer::gameOver()
{
	this->unscheduleUpdate();

	this->unschedule(SEL_SCHEDULE(&InGameLayer::gameSetp));

	this->unscheduleAllSelectors();

	//清除所有的小球
	//1.注意引用计数
}

/* 释放内存 */
void InGameLayer::distroy()
{

}

void InGameLayer::addTouchEventListener()
{
	//回调与单点触屏事件的绑定与注册
	EventListenerTouchOneByOne* touchEvent	=	EventListenerTouchOneByOne::create();
	touchEvent->onTouchBegan				=	CC_CALLBACK_2(InGameLayer::touchBegan,this);
	touchEvent->onTouchMoved				=	CC_CALLBACK_2(InGameLayer::onTouchMoved,this);
	touchEvent->onTouchEnded				=	CC_CALLBACK_2(InGameLayer::onTouchEnded,this);
	
	//事件注册
	//Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(touchEvent,0);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchEvent,this);
}

bool InGameLayer::touchBegan(Touch *touch, Event *unused_event)
{
	log("on touch began");

	//获取触摸的本地坐标
	curTouchPos	=	touch->getLocation();

	return true;//标注触屏事件 会向触屏其余事件的响应
}

void InGameLayer::onTouchMoved(Touch *touch, Event *unused_event)
{
	//获取触摸的本地坐标
	curTouchPos	=	touch->getLocation();
	log("onTouchMoved");
}

void InGameLayer::onTouchEnded(Touch *touch, Event *unused_event)
{
	log("onTouchEnded");
}

void InGameLayer::onTouchCancelled(Touch *touch, Event *unused_event)
{

}
