#include "WelcomLayer.h"
#include "InGameLayer.h"

using namespace cocos2d;

WelcomeLayer::WelcomeLayer() :	background(nullptr),
								btnItemStart(nullptr)
{

}
WelcomeLayer::~WelcomeLayer()
{

}

Scene* WelcomeLayer::createScene()
{
	auto sc		=	Scene::create();
	auto ly		=	WelcomeLayer::create();
	sc->addChild(ly);

	return sc;
}

bool WelcomeLayer::init()
{
	Layer::init();

	background		=	Sprite::create("Second21/mainFace.png");
	this->addChild(background);
	background->setAnchorPoint(Point::ZERO);

	auto norStart	=	Sprite::create("Second21/btn_start_nor.png");
	auto selStart	=	Sprite::create("Second21/btn_start_sel.png");
	btnItemStart	=	MenuItemSprite::create(norStart,selStart,nullptr,CC_CALLBACK_1(WelcomeLayer::btnTouchListener,this));
	btnItemStart->setTag(BTN_TAG_START);

	auto menu		=	Menu::create(btnItemStart,nullptr);
	//menu->addChild(btnItemStart);
	this->addChild(menu);

	return true;
}

//sender ： 点到"谁" 就是谁
void WelcomeLayer::btnTouchListener(cocos2d::Ref* sender)
{
	Node* touchObject	=	dynamic_cast<Node*>(sender);
	
	//场影 过渡
	auto sc				=	InGameLayer::createScene();
	auto transition		=	TransitionJumpZoom::create(1.5f,sc);
	Director::getInstance()->replaceScene(transition);
}