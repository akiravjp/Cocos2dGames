#ifndef _WELCOM_LAYER_H_
#define _WELCOM_LAYER_H_

#include "cocos2d.h"

class WelcomeLayer : public cocos2d::Layer
{
	typedef enum
	{
		BTN_TAG_START	=	999,
		BTN_TAG_SOUND
	}BtnTag;

public:
	WelcomeLayer();
	~WelcomeLayer();

public:

	static cocos2d::Scene* createScene();

public:

	virtual bool init();
	CREATE_FUNC(WelcomeLayer);

private:

	void btnTouchListener(cocos2d::Ref* sender);

private:

	cocos2d::Sprite* background;
	cocos2d::MenuItemSprite* btnItemStart;
};

#endif	//_WELCOM_LAYER_H_
