#include "AppDelegate.h"

#include "Layer/InGameLayer.h"
#include "Layer/WelcomLayer.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {

    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) 
	{
        glview = GLView::createWithRect("sec man",Rect(0,0,900,640));
        director->setOpenGLView(glview);
    }
	glview->setDesignResolutionSize(900, 640, ResolutionPolicy::EXACT_FIT);

    director->setDisplayStats(true);

    director->setAnimationInterval(1.0 / 60);

	auto sc	=	WelcomeLayer::createScene();

	director->runWithScene(sc);

    return true;
}

void AppDelegate::applicationDidEnterBackground() 
{
    Director::getInstance()->stopAnimation();
}

void AppDelegate::applicationWillEnterForeground()
{
    Director::getInstance()->startAnimation();
}
