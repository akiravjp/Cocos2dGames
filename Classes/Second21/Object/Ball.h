#ifndef _BALL_H_
#define _BALL_H_

#include "cocos2d.h"

class Ball : public cocos2d::Node
{
public:
	Ball(void);
	~Ball(void);

public:

	virtual bool init();
	CREATE_FUNC(Ball);

public:

	void move();
private:	
	cocos2d::Point positionAdjust(float positionX, float positionY);
private:

	float vx;
	float vy;
};

#endif //#endif

