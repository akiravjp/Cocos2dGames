#include "Player.h"

using namespace cocos2d;

Player::Player(void)
{
}


Player::~Player(void)
{
}

Player* Player::create()
{
	Player* p	=	new Player;
	if(p && p->init())
	{
		p->autorelease();
		return p;
	}
	CC_SAFE_DELETE(p);
	return nullptr;
}

bool Player::init()
{
	Sprite::init();

	initWithFile("Second21/ball_2.png");

	return true;
}

void Player::move(const Point& targetPosion)
{

	//�ȼ��ٸ���
	Point curPosition	=	this->getPosition();
	float dx				=	(targetPosion.x - curPosition.x) / 5;
	float dy				=	(targetPosion.y - curPosition.y) / 5;

	float positionX = this->getPositionX() + dx;
	float positionY = this->getPositionY() + dy;

	this->setPosition(positionAdjust(positionX, positionY));
	return ;
}

Point Player::positionAdjust(float positionX, float positionY)
{
	Size winSize	=	Director::getInstance()->getWinSize();

	float ballR = this->getContentSize().width / 2;

	if(positionX <= ballR)
	{
		positionX	=	ballR;
	}
	if(positionX >= (winSize.width - ballR))
	{
		positionX	=	(winSize.width - ballR);
	}
	if(positionY <= ballR)
	{
		positionY	=	ballR;
	}
	if(positionY >= (winSize.height - ballR))
	{
		positionY	=	(winSize.height - ballR);
	}
	
	return Point(positionX, positionY);	
}

bool Player::hitTest(Node* hitObject)
{
	//bool bl	=	this->getBoundingBox().intersectsRect(hitObject->getBoundingBox());
	//bool bl	=	this->getBoundingBox().containsPoint(Point(0,0));

	float dist	=	0;
	float dx	=	this->getPositionX() - hitObject->getPositionX();
	float dy	=	this->getPositionY() - hitObject->getPositionY();
	dist		=	sqrt(dx * dx + dy * dy);
	if(dist <= 40)
	{
		return true;
	}

	return false;
}
