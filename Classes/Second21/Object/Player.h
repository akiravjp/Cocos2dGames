#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "cocos2d.h"

class Player : public cocos2d::Sprite
{
public:
	Player(void);
	~Player(void);

public:

	virtual bool init();
	static Player* create();

public:

	void move(const cocos2d::Point& targetPosion);

	bool hitTest(cocos2d::Node* hitObject);
private:	
	cocos2d::Point positionAdjust(float positionX, float positionY);
};

#endif	//_PLAYER_H_


