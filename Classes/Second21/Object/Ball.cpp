#include "Ball.h"

using namespace cocos2d;

Ball::Ball(void) :	vx(0),
					vy(0)
{
}


Ball::~Ball(void)
{
}


bool Ball::init()
{
	Node::init();

	vx	=	CCRANDOM_0_1() * 10 - 5;//=> 0.00000001 ~0.999999999
	vy	=	CCRANDOM_0_1() * 10 - 5;
	
	auto skin	=	Sprite::create("Second21/ball_1.png");
	this->addChild(skin);
	
	this->setContentSize(skin->getContentSize());

	return true;
}

void Ball::move()
{
	float x	=	this->getPositionX() + vx;
	float y	=	this->getPositionY() + vy;

	this->setPosition(positionAdjust(x,y));
}

Point Ball::positionAdjust(float positionX, float positionY)
{
	Size winSize	=	Director::getInstance()->getWinSize();

	float ballR = this->getContentSize().width / 2;
	if(positionX <= ballR)
	{
		positionX	=	ballR;
		vx	=	-vx;
	}
	if(positionX >= (winSize.width - ballR))
	{
		positionX	=	(winSize.width - ballR);
		vx	=	-vx;
	}
	if(positionY <= ballR)
	{
		positionY	=	ballR;
		vy	=	-vy;
	}
	if(positionY >= (winSize.height - ballR))
	{
		positionY	=	(winSize.height - ballR);
		vy	=	-vy;
	}
	return Point(positionX, positionY);
}

