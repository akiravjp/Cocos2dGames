#ifndef _PANDA_H_

#define _PANDA_H_
#include "cocos2d.h"
#include "Base/Base.h"
#include "cocos/editor-support/cocostudio/CocoStudio.h"
#include "../State/PandaState.h"

class IPandaState;

class CPanda : public cocos2d::Sprite
{
public:
	friend class IPandaState;
	CPanda();
	~CPanda();
public:
	CREATE_FUNC(CPanda);
	bool init() override;	
	int getVx() const;
	void step();
	void moveY();
	void moveX();	
	bool needMoveY();
	bool needMoveX();
	void jump();	
	float getRecodeDist()const;
private:	
	void speedYLimited();
private:	
	/* 动作播放器 */
	cocostudio::Armature* m_pandaAction;
	PandaStateMachine m_stateMachine;

	/* 跳跃常量*/
	const int JUMP_VY;

	/* 正常速度 */
	const int NORMAL_VX;

	/* 重力常量 */
	const float GRAVITY;
	
	/* 虚假距离 */
	float m_recodeDist;

	/* 跳跃速度 */
	float m_vy;

	/* 跳跃所受重力 */
	float m_curGravity;
	
	/* 当前移动速度 */
	float m_vx;
	
	
	
};
#endif //_PANDA_H_

