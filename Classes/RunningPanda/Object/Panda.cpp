#include "Panda.h"

using namespace cocos2d;
using namespace cocostudio;
using namespace ui;


CPanda::CPanda():	m_pandaAction(NULL), 
					m_stateMachine(this), 
					JUMP_VY(20), 
					NORMAL_VX(12.5f),
					GRAVITY(0.98f),
					m_recodeDist(0), 
					m_curGravity(0), 
					m_vy(0), 
					m_vx(NORMAL_VX)
{
}

CPanda::~CPanda()
{
}


bool CPanda::init()
{
	Sprite::init();

	m_pandaAction = Armature::create("PandaAnimation");
	this->addChild(m_pandaAction);
	
	m_pandaAction->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM);	
	this->setPosition(100,200); 	
	//控制动画
	//art->getAnimation()->setSpeedScale(0.3);
	//art->getAnimation()->setMovementEventCallFunc();
	//art->getAnimation()->setFrameEventCallFunc();

	m_pandaAction->getAnimation()->gotoAndPause(0);
	m_stateMachine.setState(e_run);
	
	m_recodeDist = this->getPositionY();
	return true;
}

int CPanda::getVx() const
{
	return m_vx;
}

void CPanda::step()
{
	//跳跃逻辑
	m_recodeDist = m_recodeDist + m_vy;

	//递减受到的重力
	m_vy -= m_curGravity;
	speedYLimited();
}
void CPanda::speedYLimited()
{
	if(m_vy >= JUMP_VY)
	{
		m_vy = JUMP_VY;
	}
	else if(m_vy <= -JUMP_VY)
	{
		m_vy = -JUMP_VY;
	}	
}

bool CPanda::needMoveX()
{
	//横向逻辑
	float x = this->getPositionX() + m_vx;
	float countX = VisibleRect::getVisibleRect().getMaxX() / 2;
	if(x >= countX)
	{
		return false;
	}
	return true;
}
void CPanda::moveX()
{
	float x = this->getPositionX() + m_vx;	
	float countX = VisibleRect::getVisibleRect().getMaxX() / 2;

	if(x > countX)
	{
		x = countX;
	}
	this->setPositionX(x);
}
bool CPanda::needMoveY()
{
	if (m_recodeDist < VisibleRect::getVisibleRect().getMaxY() / 3 * 2)
	{
		return true;
	}
	return false;
}
void CPanda::moveY()
{
	this->setPositionY(m_recodeDist);
	return ;
}


void CPanda::jump()
{
	return m_stateMachine.jump();
}

float CPanda::getRecodeDist()const
{
	//计算差距 如果没有差距 直接设定为0 让所有背景层不动
	float y	=	VisibleRect::getVisibleRect().getMaxY()  / 3 * 2 - m_recodeDist;
	if(y < 0 )
	{
		return y;
	}
	return 0;
}

