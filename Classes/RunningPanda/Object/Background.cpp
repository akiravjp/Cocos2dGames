#include "RunningPanda/Object/Background.h"

USING_NS_CC;

Sprite *CBackground::getBackground(const std::string &file)
{
	log("CBackground::getBackground");
	
	auto ttu2dBG = Director::getInstance()->getTextureCache()->getTextureForKey(file);
	Sprite *background = Sprite::createWithTexture(ttu2dBG);
	return background;
}




