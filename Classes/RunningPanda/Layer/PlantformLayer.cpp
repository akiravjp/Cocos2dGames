#include "RunningPanda/Layer/PlantformLayer.h"


USING_NS_CC;

CPlantformLayer::CPlantformLayer(void):PLANTFORM_NUM(5)
{

}
CPlantformLayer::~CPlantformLayer(void)
{

}


Scene* CPlantformLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = CPlantformLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool CPlantformLayer::init()
{
	Layer::init();

	createPlantformList();
	
	return true;
}


void CPlantformLayer::createPlantformList()
{
	
	CPlantForm *plantForm = NULL;
	for (int i=0; i<PLANTFORM_NUM; ++i)
	{
		plantForm = createPlantform();
		this->addChild(plantForm);

		m_head.insert(plantForm->getNode());

	}
	
	ILinkNode<CPlantForm> *nextLinkNode = NULL;
	ILinkNode<CPlantForm> *curLinkNode = NULL;

	CPlantForm *curPlantForm = m_head.getNext()->getOwner();
	curPlantForm->setPosition(VisibleRect::left());

	each_link_node(&m_head, curLinkNode)
	{
		nextLinkNode = curLinkNode->getNext();
		if (nextLinkNode == &m_head)
		{
			break;
		}
		curPlantForm = curLinkNode->getOwner();
		Vec2 plantFormPosition = getPlantPosition(curPlantForm);
		nextLinkNode->getOwner()->setPosition(plantFormPosition);
	}
	return ;
}
CPlantForm *CPlantformLayer::createPlantform()
{
	StrMap strs;
	createRandKeys(strs);
	
	return CPlantForm::create(strs);
}

void CPlantformLayer::createRandKeys(StrMap &strs)
{	log("createRandKeys----------------------");
	strs.clear();
	strs[0] = "setp_1.png";
	char chstrs[32];
	int setpNum = RAND_INT(3) + 2;
	for (int i=1; i<setpNum+1; ++i)
	{
		int whitchSetp = RAND_INT(3) + 2;
		snprintf(chstrs, sizeof(chstrs), "setp_%d.png", whitchSetp);
		log("setp  %s", chstrs);
		strs[i] = chstrs;
	}

	strs[setpNum+1] = "setp_5.png";
	return ;	
}

void CPlantformLayer::move(float vx)
{	//log("move --------------------");
	CPlantForm *curPlantForm = NULL;
	ILinkNode<CPlantForm> *curLinkNode = NULL;
	
	each_link_node(&m_head, curLinkNode)
	{
		CPlantForm *curPlantForm = curLinkNode->getOwner();
		float positionX = curPlantForm->getPositionX() - vx;
		
		curPlantForm->setPositionX(positionX);
		if (positionX < -(curPlantForm->getWidth() + 100))
		{
			curLinkNode = curLinkNode->getNext();			
			dealOutOfRange(curPlantForm);

			curPlantForm = curLinkNode->getOwner();
			positionX = curPlantForm->getPositionX() - vx;
			curPlantForm->setPositionX(positionX);
		}
	}

	return ;
}

void CPlantformLayer::dealOutOfRange(CPlantForm *plantForm)
{
	if (!plantForm)
	{
		return ;
	}
	
	plantForm->getNode()->remove();
	this->removeChild(plantForm);

	
	plantForm = createPlantform();
	this->addChild(plantForm);
	//log("plantForm  %p", plantForm);
	CPlantForm *afterWitch = m_head.getPre()->getOwner();
	afterWitch->getNode()->insert(plantForm->getNode());

	Vec2 plantFormPositon = getPlantPosition(afterWitch);
	plantForm->setPosition(plantFormPositon);
	return ;
}

Vec2 CPlantformLayer::getPlantPosition(CPlantForm *afterWitch)
{
	float positionX = afterWitch->getPositionX() + afterWitch->getWidth() + 100 + RAND_INT(200);
	float positionY = afterWitch->getPositionY() + CCRANDOM_0_1() * 200 - 100;
	//log("getPlantPosition  positionX, positionY  %p  %f  %f", afterWitch, positionX, positionY);

	if(positionY < 100 || positionY > VisibleRect::getVisibleRect().getMaxY() / 3 * 2)
	{
		positionY =	150;
	}

	return Vec2(positionX, positionY);
}

