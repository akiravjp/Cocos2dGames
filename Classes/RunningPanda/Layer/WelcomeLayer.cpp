#include "RunningPanda/Layer/WelcomeLayer.h"
#include "RunningPanda/Layer/InGameLayer.h"


#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;

WelcomeLayer::WelcomeLayer(void):	m_welcomeWid(NULL)
{
}


WelcomeLayer::~WelcomeLayer(void)
{
}

Scene* WelcomeLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = WelcomeLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool WelcomeLayer::init()
{
	Layer::init();
	initBackground();	
	initCocosStudio();
	return true;
}

void WelcomeLayer::initBackground()
{
	//添加背景
	//使用异步加载过来的背景
	auto ttu2dBG = Director::getInstance()->getTextureCache()->getTextureForKey("RunningPanda/background/background.png");
	auto background = Sprite::createWithTexture(ttu2dBG);
	this->addChild(background);
	background->setAnchorPoint(Point::ZERO);
	return ;
}

void WelcomeLayer::initCocosStudio()
{
	m_welcomeWid = GUIReader::getInstance()->widgetFromJsonFile("RunningPanda/WelcomeLayerUI/WelcomeLayerUI.ExportJson");
	this->addChild(m_welcomeWid);

	
	auto* btnStart = dynamic_cast<Button*>(m_welcomeWid->getChildByName("btnStart"));
	auto* btnOption = dynamic_cast<Button*>(m_welcomeWid->getChildByName("btnOption"));
	auto* btnRank = dynamic_cast<Button*>(m_welcomeWid->getChildByName("btnRank"));
	auto* btnStore = dynamic_cast<Button*>(m_welcomeWid->getChildByName("btnStore"));

	btnStart->addTouchEventListener(CC_CALLBACK_2(WelcomeLayer::menuOnTouch, this));
	btnOption->addTouchEventListener(CC_CALLBACK_2(WelcomeLayer::menuOnTouch, this));
	btnRank->addTouchEventListener(CC_CALLBACK_2(WelcomeLayer::menuOnTouch, this));
	btnStore->addTouchEventListener(CC_CALLBACK_2(WelcomeLayer::menuOnTouch, this));

	return ;
}

void WelcomeLayer::onEnterTransitionDidFinish()
{
	Layer::onEnterTransitionDidFinish();
}

void WelcomeLayer::menuOnTouch(Ref *pSender, Widget::TouchEventType type)
{
	std::string btnName = ((Node *)pSender)->getName();

	log("btnName  %s", btnName.c_str());
	
	switch(type)
	{
		case Widget::TouchEventType::BEGAN:
			log("btnTouchEvent  BEGAN");
			break;
		case Widget::TouchEventType::MOVED:
			log("btnTouchEvent  MOVED");
			break;
		case Widget::TouchEventType::ENDED:
			log("btnTouchEvent  ENDED");
			if (btnName == "btnStart")
			{
				gameStart();
			}

			break;
		case Widget::TouchEventType::CANCELED:
			log("btnTouchEvent  CANCELED");
			break;
		default:
			log("btnTouchEvent  default");
			break;

	}
	return ;
}

void WelcomeLayer::gameStart()
{
	log("game start !!!");
	Scene *inGameLayerScene= InGameLayer::createScence();

	//replaceScene 在场景切换完之后， 将之前的场景销毁
	Director::getInstance()->replaceScene(TransitionProgressHorizontal::create(0.5f, inGameLayerScene));

	return ;
}

