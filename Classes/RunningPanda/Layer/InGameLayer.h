#ifndef _INGAME_LAYER_H_
#define _INGAME_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

#include "RunningPanda/Layer/BackgroundLayer.h"
#include "RunningPanda/Layer/PlantformLayer.h"
#include "RunningPanda/Object/Panda.h"


class IInGameState;
class InGameLayer;

typedef enum
{
	e_stateWait, 
	e_stateReady,
	e_statePlay, 
	e_stateOver,	
}E_STATE;

class InGameStateMachine : public IStateMachine<IInGameState>
{
public:
	InGameStateMachine(InGameLayer *pInGameLayer);
private:	
	friend class IInGameState;
	//标明该状态机属于谁
	InGameLayer *m_inGameLayer;
public: 
	//动作列表
	void runInGame();
};



class InGameLayer:public cocos2d::Layer
{
public:
	InGameLayer(void);
	~InGameLayer(void);
	friend class IInGameState;
public:
	virtual bool init();
	void onEnterTransitionDidFinish() override;
	//create();
	CREATE_FUNC(InGameLayer);
	static cocos2d::Scene* createScence();
	void gameStep(float dt);
private:
	void addEvents();
	virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event);
private:
	CBackgroundLayer *m_background;
	CPlantformLayer *m_plantform;
	InGameStateMachine m_inGameStateMachine;
	CPanda *m_panda;
};


class IInGameState 
{
protected:
	InGameStateMachine *m_gameMachine;
	InGameLayer *&m_inGameLayer;
public:
	IInGameState(InGameStateMachine *pGameMachine);
	virtual ~IInGameState() = 0 {}	
public:
	virtual void runInGame(){}
protected:
	CBackgroundLayer *&m_background;
	CPlantformLayer *&m_plantform;
	CPanda *&m_panda;
};
//-----------------------------------------------

class CGameWait : public IInGameState
{
public:
	CGameWait(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
};

class CGameReady : public IInGameState
{
public:
	CGameReady(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
};
class CGamePlay : public IInGameState
{
public:
	CGamePlay(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
public:
	void runInGame() override;
private:
	void PlantAndBackGroundMoveY();	
};
class CGameOver : public IInGameState
{
public:
	CGameOver(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
};


#endif	//_INGAME_LAYER_H_

