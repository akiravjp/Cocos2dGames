#include "RunningPanda/Layer/LoadingLayer.h"
#include "RunningPanda/Layer/WelcomeLayer.h"
#include "cocos/editor-support/cocostudio/CocoStudio.h"
#include "cocos/ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

const char *pandaAnimation = "RunningPanda/PandaAnimation/PandaAnimation.ExportJson";
const char *backGroundStr[5] = {
						"background.png", 
						"hillViewFar.png", 
						"hillViewNear.png", 
						"treeViewFar.png", 
						"treeViewNear.png", 
	};


CLoadingLayer::CLoadingLayer(void):	m_totalCount(0), 
										m_loadedCount(0), 
										m_labelLoading(NULL), 
										m_labelPercent(NULL)
{
}


CLoadingLayer::~CLoadingLayer(void)
{
}

Scene* CLoadingLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = CLoadingLayer::create();
	
	sc->addChild(ly);

	return sc;
}


bool CLoadingLayer::init()
{
	Layer::init();
	initBackground();
	initProgress();
	initResources();
	return true;
}

void CLoadingLayer::initResources()
{
	auto dataManager = ArmatureDataManager::getInstance();
	dataManager->addArmatureFileInfoAsync(pandaAnimation, this, schedule_selector(CLoadingLayer::armtureCompeleteEvent));
	++m_totalCount;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("RunningPanda/element.plist");

	char fileName[64];
	auto textureCache = Director::getInstance()->getTextureCache();
	int forSize = sizeof(backGroundStr)/sizeof(char *);
	for (int i=0; i<forSize; ++i)
	{
		snprintf(fileName, sizeof(fileName), "RunningPanda/background/%s", backGroundStr[i]);		
		textureCache->addImageAsync(fileName, CC_CALLBACK_1(CLoadingLayer::textureDataLoadedEvent, this));
		++m_totalCount;
	}
	
	return ;
}

void CLoadingLayer::initBackground()
{
	Sprite *background = NULL;

	background = Sprite::create("RunningPanda/background/loading.png");
	this->addChild(background);
	background->setAnchorPoint(Point::ZERO);

	return ;
}
void CLoadingLayer::initProgress()
{
	auto size = Director::getInstance()->getWinSize();

    m_labelLoading = Label::createWithTTF("loading...", "RunningPanda/fonts/arial.ttf", 15);
    m_labelPercent = Label::createWithTTF("%0", "RunningPanda/fonts/arial.ttf", 15);

    m_labelLoading->setPosition(Vec2(size.width / 2, size.height / 2 - 20));
    m_labelPercent->setPosition(Vec2(size.width / 2, size.height / 2 + 20));

    this->addChild(m_labelLoading);
    this->addChild(m_labelPercent);

	return ;
}

void CLoadingLayer::setProgress(float percent)
{
    char tmp[10];
    sprintf(tmp,"%%%d", (int)(((float)m_loadedCount / m_totalCount) * 100));
    m_labelPercent->setString(tmp);

	return ;
}

void CLoadingLayer::armtureCompeleteEvent(float percent)
{
	if (percent == 1)
	{
		++m_loadedCount;
	}

	float loadingPercent = m_loadedCount / m_totalCount;
	setProgress(loadingPercent);
	
	changeScenen();	
	return ;
}

void CLoadingLayer::textureDataLoadedEvent(Texture2D *ttu2d)
{	
	m_loadedCount++;
	
	float percent = m_loadedCount / m_totalCount;
	setProgress(percent);
	
	changeScenen();
	return ;
}

void CLoadingLayer::changeScenen()
{
	if (m_totalCount == m_loadedCount)
	{
		Director::getInstance()->replaceScene(TransitionProgressOutIn::create(0.5f, WelcomeLayer::createScence()));
	}
	return ;
}

