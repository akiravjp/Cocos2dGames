#include "RunningPanda/Layer/BackgroundLayer.h"
#include "RunningPanda/Layer/LoadingLayer.h"


USING_NS_CC;
CBackgroundLayer::CBackgroundLayer(void):m_speed(1)
{
	return ;
}


CBackgroundLayer::~CBackgroundLayer(void)
{
}

Scene* CBackgroundLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = CBackgroundLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool CBackgroundLayer::init()
{
	Layer::init();
	char fileName[64];
	StrMap fileNames;

	int forSize = sizeof(backGroundStr)/sizeof(char *);
	for (int i=0; i<forSize; ++i)
	{
		snprintf(fileName, sizeof(fileName), "RunningPanda/background/%s", backGroundStr[i]);

		fileNames[0] = fileName;
		fileNames[1] = fileName;		
		
		m_backgroundMap[i] = CBackground::create(fileNames);
		CBackground *backgrounds = m_backgroundMap[i];
		
		this->addChild(backgrounds);

		int positionY = 150 - (50 * i);
		backgrounds->setPositionY(positionY);
		backgrounds->setAnchorPoint(Point::ZERO);
		
	}
	return true;
}

void CBackgroundLayer::moveLeft(float speed)
{
	if (speed != -1)
	{
		m_speed = speed;
	}
	for (unsigned int i=0; i<m_backgroundMap.size(); ++i)
	{
		CBackground *background = m_backgroundMap[i];
		background->moveLeft(m_speed*(i+1)*0.2f);
	}
	return ;
}


