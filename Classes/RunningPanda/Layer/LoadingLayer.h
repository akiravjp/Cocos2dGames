#ifndef _LOADING_LAYER_H_
#define _LOADING_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

extern const char *backGroundStr[5];
extern const char *pandaAnimation;

class CLoadingLayer:public cocos2d::Layer
{
public:
	CLoadingLayer(void);
	~CLoadingLayer(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(CLoadingLayer);
	static cocos2d::Scene* createScence();
private:
	//�첽ͼƬ���ػص�
	void textureDataLoadedEvent(cocos2d::Texture2D *ttu2d);	
	void armtureCompeleteEvent(float percent);
	void changeScenen();
	void initResources();
	void initBackground();
	void initProgress();
	void setProgress(float percent);
private:
	float m_totalCount;
	float m_loadedCount;

	cocos2d::Label *m_labelLoading;
	cocos2d::Label *m_labelPercent;

};

#endif	//_LOADING_LAYER_H_
