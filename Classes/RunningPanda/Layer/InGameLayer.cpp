#include "RunningPanda/Layer/InGameLayer.h"



USING_NS_CC;
InGameLayer::InGameLayer(void):	m_inGameStateMachine(this), 
									m_background(NULL), 
									m_plantform(NULL), 
									m_panda(NULL)
{
}


InGameLayer::~InGameLayer(void)
{
}



Scene* InGameLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = InGameLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool InGameLayer::init()
{
	Layer::init();

	m_background = CBackgroundLayer::create();
	this->addChild(m_background);

	m_plantform = CPlantformLayer::create();
	this->addChild(m_plantform);

	m_panda = CPanda::create();
	this->addChild(m_panda);
		
	return true;
}

void InGameLayer::onEnterTransitionDidFinish()
{
	Layer::onEnterTransitionDidFinish();

	this->schedule(SEL_SCHEDULE(&InGameLayer::gameStep),0.02f);
	addEvents();

}

void InGameLayer::addEvents()
{
	log("addEvents !!!");
	auto touchEvt = EventListenerTouchOneByOne::create();
	touchEvt->onTouchBegan = CC_CALLBACK_2(InGameLayer::onTouchBegan, this);
	touchEvt->onTouchMoved = CC_CALLBACK_2(InGameLayer::onTouchMoved, this);
	touchEvt->onTouchEnded = CC_CALLBACK_2(InGameLayer::onTouchEnded, this);
	touchEvt->onTouchCancelled = CC_CALLBACK_2(InGameLayer::onTouchCancelled, this);

	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchEvt, this);
	return ;
}


bool InGameLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchBegan !!!");

	m_panda->jump();
	//ture 后续的三个事件回调将会响应
	//false后续的三个事件回调将不再响应
	return true;
}

void InGameLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchMoved !!!");
}

void InGameLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchEnded !!!");

}
void InGameLayer::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchCancelled !!!");

}



//时间步进器
void InGameLayer::gameStep(float dt)
{
	m_inGameStateMachine.runInGame();
	return ;
}


InGameStateMachine::InGameStateMachine(InGameLayer *pInGameLayer):m_inGameLayer(pInGameLayer)
{
	insertMap(e_statePlay, new CGamePlay(this));
	insertMap(e_stateWait, new CGameWait(this));
	insertMap(e_stateReady, new CGameReady(this));
	insertMap(e_stateOver, new CGameOver(this));
}

void InGameStateMachine::runInGame()
{
	return m_state->runInGame();
}


IInGameState::IInGameState(InGameStateMachine *pGameMachine):
														m_gameMachine(pGameMachine), 
														m_inGameLayer(m_gameMachine->m_inGameLayer), 
														m_background(m_inGameLayer->m_background), 
														m_plantform(m_inGameLayer->m_plantform), 
														m_panda(m_inGameLayer->m_panda)
{
	return ;
}


void CGamePlay::runInGame()
{
	if (m_panda->needMoveY())
	{
		m_panda->moveY();
	}
	else
	{
		PlantAndBackGroundMoveY();
	}
	if (m_panda->needMoveX())
	{
		m_panda->moveX();
	}
	else
	{
		float pandaSpeed = m_panda->getVx();
		m_panda->step();	
		m_background->moveLeft(pandaSpeed); 
		m_plantform->move(pandaSpeed);
	}

	return ;
}


void CGamePlay::PlantAndBackGroundMoveY()
{
	float recodeDist = m_panda->getRecodeDist();
	m_plantform->setPositionY(recodeDist);		
	m_background->setPositionY(recodeDist);
	return ;
}


