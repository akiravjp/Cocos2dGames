#ifndef _WELCOME_LAYER_H_
#define _WELCOME_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"
#include "cocos/editor-support/cocostudio/CocoStudio.h"
#include "cocos/ui/CocosGUI.h"

class WelcomeLayer:public cocos2d::Layer
{
public:
	WelcomeLayer(void);
	~WelcomeLayer(void);
	virtual bool init();
	//create();
	CREATE_FUNC(WelcomeLayer);
	static cocos2d::Scene* createScence();
private:
	void initBackground();
	void initCocosStudio();
	//�л�����
	virtual void onEnterTransitionDidFinish();
	void menuOnTouch(cocos2d::Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
	void gameStart();
private:
	cocos2d::ui::Widget *m_welcomeWid;
};

#endif	//_WELCOME_LAYER_H_
