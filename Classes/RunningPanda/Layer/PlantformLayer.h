#ifndef _PLANTFORM_LAYER_H_
#define _PLANTFORM_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

class CPlantForm;
class CPlantformLayer:public cocos2d::Layer
{
public:
	CPlantformLayer(void);
	~CPlantformLayer(void);
public:
	bool init() override;
	//create();
	CREATE_FUNC(CPlantformLayer);
	static cocos2d::Scene* createScence();
	void move(float vx);
private:
	CPlantForm *createPlantform();
	void createPlantformList();
	void createRandKeys(StrMap &strs);
	void dealOutOfRange(CPlantForm *plantForm);
	cocos2d::Vec2 getPlantPosition(CPlantForm *afterWitch);
private:
	const int PLANTFORM_NUM;
	ILinkNode<CPlantForm> m_head;

};

class CPlantForm: public IPlantform
{
public:
	CPlantForm():m_linkNode(this){};
	CREATE_PLANTFORM(CPlantForm);
public:
	ILinkNode<CPlantForm> *getNode(){return &m_linkNode;}
private:
	ILinkNode<CPlantForm> m_linkNode;
};



#endif	//_PLANTFORM_LAYER_H_



