#ifndef _BACKGROUND_LAYER_H_
#define _BACKGROUND_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"
#include "RunningPanda/Object/Background.h"

class CBackgroundLayer:public cocos2d::Layer
{
public:
	CBackgroundLayer(void);
	~CBackgroundLayer(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(CBackgroundLayer);
	void moveLeft(float speed = -1);
	void setSpeed(float speed){m_speed = speed;};
	static cocos2d::Scene* createScence();
private:	
	typedef std::map<int, CBackground*> MapBackground;
	MapBackground m_backgroundMap;

	float m_speed;
	
};

#endif	//_BACKGROUND_LAYER_H_
