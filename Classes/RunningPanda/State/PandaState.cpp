#include "PandaState.h"
#include "RunningPanda/Object/Panda.h"
#include "cocos2d.h"


using namespace cocos2d;

PandaStateMachine::PandaStateMachine(CPanda *pPanda):m_panda(pPanda)
{

	insertMap(e_idoe, new CPandaIdoe(this));
	insertMap(e_run, new CPandaRun(this));
	insertMap(e_jump1, new CPandaJump1(this));
	insertMap(e_jump2, new CPandaJump2(this));
	insertMap(e_fly, new CPandaFly(this));
	insertMap(e_down, new CPandaDown(this));
}


void PandaStateMachine::jump()
{
	return m_state->jump();
}



IPandaState::IPandaState(PandaStateMachine *pPandaMachine):
														m_pandaMachine(pPandaMachine), 
														m_panda(pPandaMachine->m_panda), 
														m_vx(m_panda->m_vx), 
														m_vy(m_panda->m_vy), 
														m_curGravity(m_panda->m_curGravity), 
														m_pandaAction(m_panda->m_pandaAction), 
														m_recodeDist(m_panda->m_recodeDist), 
														NORMAL_VX(m_panda->NORMAL_VX), 
														JUMP_VY(m_panda->JUMP_VY), 
														GRAVITY(m_panda->GRAVITY)
{
	return ;
}


void CPandaIdoe::jump()
{
	log("state -> idoe");
	m_vy = 0;
	m_vx = 0;
	m_curGravity = 0;	
	m_pandaAction->getAnimation()->gotoAndPause(0);
	m_recodeDist = m_panda->getPositionY(); 

	m_pandaMachine->setState(e_run);
	return ;
}

void CPandaRun::jump()
{
	log("state -> run");
	m_vy = 0;
	m_vx = NORMAL_VX;
	m_curGravity = 0;	
	m_pandaAction->getAnimation()->play("run");

	m_pandaMachine->setState(e_jump1);
	return ;
}


void CPandaJump1::jump()
{
	log("state -> one");
	m_vy = JUMP_VY;
	m_curGravity = GRAVITY;	
	m_pandaAction->getAnimation()->play("jumpOne");
	
	m_pandaMachine->setState(e_jump2);
	return ;
}

void CPandaJump2::jump()
{
	log("state -> double");
	m_vy = JUMP_VY;
	m_curGravity = GRAVITY;
	m_pandaAction->getAnimation()->play("jumpTwo");

	m_pandaMachine->setState(e_fly);
	return ;
}

void CPandaFly::jump()
{
	log("state -> fly");
	m_curGravity = -GRAVITY;
	m_pandaAction->getAnimation()->play("fly");

	m_pandaMachine->setState(e_fly);
	return ;
}

void CPandaDown::jump()
{
	log("state -> down");
	m_curGravity = GRAVITY;

	return ;
}



