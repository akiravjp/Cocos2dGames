#ifndef _PANDA_STATE_H_

#define _PANDA_STATE_H_
#include "Base/Base.h"

class CPanda;
class IPandaState;

typedef enum
{
	e_idoe, 
	e_run, 
	e_jump1, 
	e_jump2, 
	e_fly, 
	e_down
} PandaState;

class PandaStateMachine : public IStateMachine<IPandaState>
{
public:
	PandaStateMachine(CPanda *pPanda);
private:	
	friend class IPandaState;
	//标明该状态机属于谁
	CPanda *m_panda;
public: 
	//动作列表
	void jump();

};


class IPandaState 
{
protected:
	PandaStateMachine *m_pandaMachine;
	CPanda *&m_panda;
public:
	IPandaState(PandaStateMachine *pPandaMachine);
	virtual ~IPandaState() = 0 {}	
public:
	virtual void jump(){};
protected:
	float &m_vx;
	float &m_vy;
	float &m_curGravity;
	cocostudio::Armature *&m_pandaAction;
	float &m_recodeDist;
	const int &NORMAL_VX;
	/* 跳跃常量*/	
	const int &JUMP_VY;
	const float &GRAVITY;
};


class CPandaIdoe : public IPandaState
{
public:
	CPandaIdoe(PandaStateMachine *pPandaMachine):IPandaState(pPandaMachine){}
public:
	void jump() override;
};

class CPandaRun : public IPandaState
{
public:
	CPandaRun(PandaStateMachine *pPandaMachine):IPandaState(pPandaMachine){}
public:
	void jump() override;
	
};
class CPandaJump1 : public IPandaState
{
public:
	CPandaJump1(PandaStateMachine *pPandaMachine):IPandaState(pPandaMachine){}
public:
	void jump() override;

};
class CPandaJump2 : public IPandaState
{
public:
	CPandaJump2(PandaStateMachine *pPandaMachine):IPandaState(pPandaMachine){}
public:
	void jump() override;	
};
class CPandaFly : public IPandaState
{
public:
	CPandaFly(PandaStateMachine *pPandaMachine):IPandaState(pPandaMachine){}
public:
	void jump() override;	
};
class CPandaDown : public IPandaState
{
public:
	CPandaDown(PandaStateMachine *pPandaMachine):IPandaState(pPandaMachine){}
public:
	void jump() override;

};

#endif
