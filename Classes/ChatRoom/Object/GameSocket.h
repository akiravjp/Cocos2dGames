#ifndef __GAME_SOCKET_H__
#define __GAME_SOCKET_H__

#ifdef WIN32
#include <WinSock2.h>
#else
#include <sys/socket.h>
#endif

class CGameSocket
{
public:
	static CGameSocket * get();
public:
	CGameSocket();
	~CGameSocket();
public:
	bool init();
	bool connect();
	void close();
	bool send( const char * buf, unsigned int len );
	char * getBuf();
	void run();
	bool isRead() { return m_bRead; }
private:
	SOCKET m_socketClient;
	char m_bufRecv[256];
	bool m_bRead;
};

#endif//__GAME_SOCKET_H__