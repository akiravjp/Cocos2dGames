#include "WelcomLayer.h"
#include "../Object/GameSocket.h"

using namespace cocos2d;

WelcomeLayer::WelcomeLayer() :	m_pLblShow(nullptr)									
{

}
WelcomeLayer::~WelcomeLayer()
{

}

Scene* WelcomeLayer::createScene()
{
	auto sc		=	Scene::create();
	auto ly		=	WelcomeLayer::create();
	sc->addChild(ly);

	return sc;
}

bool WelcomeLayer::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}
	
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//	  you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
										   "ChatRoom/CloseNormal.png",
										   "ChatRoom/CloseSelected.png",
										   CC_CALLBACK_1(WelcomeLayer::menuCloseCallback, this));
	closeItem->setTag( 1 );
	
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
								origin.y + closeItem->getContentSize().height/2));

	auto connect = MenuItemImage::create( "ChatRoom/CloseNormal.png",
										  "ChatRoom/CloseSelected.png",
										  CC_CALLBACK_1(WelcomeLayer::menuCloseCallback, this));
	connect->setTag( 2 );
	
	connect->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
								origin.y + closeItem->getContentSize().height/2+100));

	auto send = MenuItemImage::create( "ChatRoom/CloseNormal.png",
									   "ChatRoom/CloseSelected.png",
									   CC_CALLBACK_1(WelcomeLayer::menuCloseCallback, this));
	send->setTag( 3 );
	
	send->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
								origin.y + closeItem->getContentSize().height/2+200));

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, connect, send, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label
	
	m_pLblShow = LabelTTF::create("Hello World", "Arial", 24);
	
	// position the label on the center of the screen
	m_pLblShow->setPosition(VisibleRect::center());

	// add the label as a child to this layer
	this->addChild(m_pLblShow, 1);

	// add "WelcomeLayer" splash screen"
	auto sprite = Sprite::create("ChatRoom/HelloWorld.png");

	// position the sprite on the center of the screen
	sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

	// add the sprite as a child to this layer
	this->addChild(sprite, 0);

	CGameSocket::get()->init();

	this->schedule(schedule_selector(WelcomeLayer::scanMsgQueue), 0.5);
	return true;
}


void WelcomeLayer::menuCloseCallback(Ref* pSender)
{
	Node * sender = dynamic_cast< Node * >( pSender );
	if( NULL == sender )
	{
		return;
	}

	switch( sender->getTag() )
	{
	case 1:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
		MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
		return;
#endif

		Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		exit(0);
#endif
		break;

	case 2:
		if( CGameSocket::get()->connect() )
		{
			CCLOG( "connect ok!" );
		}
		else
		{
			CCLOG( "connect error!" );
			CGameSocket::get()->close();
		}
		break;
	case 3:
		if( CGameSocket::get()->send( "aaa", 3 ) ) 
		{
			m_pLblShow->setString( CGameSocket::get()->getBuf() );
		}
		else
		{
			CCLOG( "connect error!" );
		}
		break;
	}
}

void WelcomeLayer::scanMsgQueue( float delta )
{
	CGameSocket::get()->run();
	if( CGameSocket::get()->isRead() )
	{
		m_pLblShow->setString( CGameSocket::get()->getBuf() );
	}
}


