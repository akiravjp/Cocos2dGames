#ifndef BASE_DEF_H
#define BASE_DEF_H

#include "cocos2d.h"
USING_NS_CC;


#define MAX_RES_COUNT 1

typedef enum EGameStateType
{
	EGameStateType_Start,
	EGameStateType_Playing,
	EGameStateType_GameOver,
	EGameStateType_ReStart,
	EGameStateType_Max
};




#endif