#include "MaskScene.h"


Scene* MaskScene::Scene()
{
	CCScene* TScene = Scene::create();
	MaskScene* layer = MaskScene::create();
	TScene->addChild(layer);
	return TScene;


}

bool MaskScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 visibleOrigin = Director::getInstance()->getVisibleOrigin();

	//设置裁剪区域
	clip = ClippingNode::create();
	//设置镂空阈值
	clip->setInverted(true);
	this->addChild(clip);

	//添加颜色层
	auto backLayer = LayerColor::create(Color4B(0, 0, 0, 180));
	//this->addChild(backLayer);
	clip->addChild(backLayer);

	//drawRectangularHelp();
	drawCircularHelp();

	return true;
}

void MaskScene::drawRectangularHelp()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//添加遮罩模型
	auto TDrowNode = DrawNode::create();
	Color4F yellow(1, 1, 0, 1);
	Vec2 rec[4] = { Vec2(-40.f, 30.f), Vec2(30.f, 30.f), Vec2(30.f, -26.f), Vec2(-40.f, -26.f) };
	TDrowNode->drawPolygon(rec, 4, yellow, 0, yellow);
	TDrowNode->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	clip->setStencil(TDrowNode);

}

void MaskScene::drawCircularHelp()
{
	//添加圆形遮罩模型
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Color4F red(1,0,0,1);
	float radius = 50.f;	//圆半径
	const int iCount = 300;
	const float angle = 2.f*(float)M_PI / iCount;	//弧度
	Vec2 circle[iCount];	//定点

	for (int i = 0; i < iCount; ++i)
	{
		float radian = i*angle;	//弧度
		circle[i].x = radius*cosf(radian);
		circle[i].y = radius*sinf(radian);
	}

	auto TDrowNode = DrawNode::create();
	TDrowNode->drawPolygon(circle, iCount, red, 0, red);//绘制多边形
	TDrowNode->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	clip->setStencil(TDrowNode);	//设置模板

	auto act1 = ScaleTo::create(0.05f, 0.95f);
	auto act2 = ScaleTo::create(0.2f, 1.f);
	auto act3 = Sequence::create(act1, act2, NULL);
	auto actResult = RepeatForever::create(act3);
	TDrowNode->runAction(actResult);
}

