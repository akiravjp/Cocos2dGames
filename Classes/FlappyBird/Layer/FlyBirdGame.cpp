#include"FlyBirdGame.h"
#include"../resource.h"

FlyBirdGame::FlyBirdGame() : score(0), 
								gravity(0.2), 
								velocity(-2)
{

}

cocos2d::Scene* FlyBirdGame::createScene()
{
	//const char* name = "hello.png";
	//Sprite::create(name);
	auto	scene = Scene::create();
	auto	layer = FlyBirdGame::create();
	scene->addChild(layer);
	return scene;
}

enum FlyUPPartType
{
	FlyUPPartType_BG,
	FlyUPPartType_StartBtn,
	FlyUPPartType_LOGO,
	FlyUPPartType_Hero,
	FlyUPPartType_Score,
	FlyUPPartType_GameOver,
	FlyUPPartType_Max
};

bool FlyBirdGame::init()
{
	if (!Layer::init())
	{
		return false;
	}
	m_GameState = EGameStateType_Start;
	if (!initUI())
	{
		return false;
	}



	return true;
}

bool FlyBirdGame::initUI()
{
	auto winSize = Director::getInstance()->getWinSize();
	Vec2 centerPos(winSize.width/2, winSize.height/2);
	
	auto bg = Sprite::create(bird_bg);
	bg->setPosition(winSize.width/2, winSize.height/2);
	bg->setScale(winSize.width/bg->getContentSize().width, 
	winSize.height/bg->getContentSize().height);
	bg->setTag(FlyUPPartType_BG);
	this->addChild(bg);

	auto logo = Sprite::createWithSpriteFrameName("bird_logo.png");
	logo->setTag(FlyUPPartType_LOGO);
	logo->setPosition(winSize.width / 2, winSize.height / 2 + logo->getContentSize().height * 2);
	this->addChild(logo);

	auto startBtn = MenuItemImage::create("FlappyBird/bird_start_btn.png",
		"FlappyBird/brid_start_btn_pressed.png",
		CC_CALLBACK_1(FlyBirdGame::gameStart, this));
	auto menu = Menu::create(startBtn, NULL);
	menu->setTag(FlyUPPartType_StartBtn);
	this->addChild(menu);

	//game over
	auto overLogo = Sprite::create(bird_gameover);
	overLogo->setPosition(Vec2(centerPos.x, centerPos.y + 100.f));
	overLogo->setTag(FlyUPPartType_GameOver);
	overLogo->setVisible(false);
	this->addChild(overLogo);


	auto hero = Sprite::create(bird_hero);
	hero->setPosition(winSize.width / 3, winSize.height / 2);
	hero->setTag(FlyUPPartType_Hero);
	hero->setVisible(false);
	this->addChild(hero, 1);

	Animation* an = Animation::create();
	an->addSpriteFrameWithFileName(bird_hero);
	an->addSpriteFrameWithFileName(bird_hero2);
	an->addSpriteFrameWithFileName(bird_hero3);
	an->setDelayPerUnit(0.5f/3.f);
	an->setLoops(-1);

	auto anim = Animate::create(an);
	hero->runAction(anim);


	//touch
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesBegan = CC_CALLBACK_2(FlyBirdGame::onTouchesBegin, this);
	listener->onTouchesEnded = CC_CALLBACK_2(FlyBirdGame::onTouchesEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	scheduleUpdate();
	return true;
}

void FlyBirdGame::update(float dt)
{
	auto size = Director::getInstance()->getVisibleSize();
	auto hero = this->getChildByTag(FlyUPPartType_Hero);
	switch (m_GameState)
	{
	case EGameStateType_Start:
		break;
	case EGameStateType_Playing:
		if (hero->getPositionY() > 0 && hero->getPositionY()<size.height)
		{
			velocity -= gravity;
			hero->setPositionY(hero->getPositionY() + velocity);
		}
		break;
	case EGameStateType_GameOver:
		this->getChildByTag(FlyUPPartType_GameOver)->setVisible(true);
		break;
	case EGameStateType_ReStart:
	{
		//Ӣ�ۻظ�ԭλ
		hero->setPosition(Vec2( size.width/5, size.height*0.8f) );
		hero->setVisible(true);
		//�ָ�UI
		this->getChildByTag(FlyUPPartType_StartBtn)->setVisible(true);
		this->getChildByTag(FlyUPPartType_LOGO)->setVisible(true);
		//this->getChildByTag(FlyUPPartType_Score)->setVisible(true);
		this->getChildByTag(FlyUPPartType_GameOver)->setVisible(false);
	}
		break;
	case EGameStateType_Max:
		break;
	default:
		break;
	}
}

void FlyBirdGame::gameStart(Object* pSender)
{
	log("start");
	this->getChildByTag(FlyUPPartType_Hero)->setVisible(true);
	this->getChildByTag(FlyUPPartType_StartBtn)->setVisible(false);
	this->getChildByTag(FlyUPPartType_LOGO)->setVisible(false);

	m_GameState = EGameStateType_ReStart;
	velocity = -3;
}

void FlyBirdGame::onTouchesBegin(const std::vector<Touch*>& touches, Event* event)
{
	log("FlyBirdGame::onTouchesBegin");
	if (m_GameState == EGameStateType_Playing)
	{
		velocity = 5;
	}

}

void FlyBirdGame::onTouchesEnded(const std::vector<Touch*>& touches, Event* event)
{
	log("FlyBirdGame::onTouchesEnded");
}


