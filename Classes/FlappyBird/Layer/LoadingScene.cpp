#include "LoadingScene.h"
#include "../resource.h"
#include "FlyBirdGame.h"
#include "MaskScene.h"

bool LoadingScene::init()
{
	if (!Layer::init())
	{
		return false;
	}
	m_iLoadedResCount = 0;
	if (!initUI())
	{
		return false;
	}

	scheduleUpdate();
	return true;
}

void LoadingScene::update(float dt)
{

	if (m_iLoadedResCount == MAX_RES_COUNT)
	{
		changeScene();
	}
}

bool LoadingScene::initUI()
{
	Size visibSize = Director::getInstance()->getWinSize();
	Vec2 centerPos(visibSize.width/2, visibSize.height/2);

	auto progress1 = ProgressTo::create(3.f, 100);

	auto m_sprite1 = Sprite::create("FlappyBird/jindutiao.png");
	auto m_progressTime1 = ProgressTimer::create(m_sprite1);
	m_progressTime1->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	m_progressTime1->setPosition(centerPos);
	m_progressTime1->setPercentage(0);
	m_progressTime1->setScale(0.5f);
	this->addChild(m_progressTime1);

	m_progressTime1->setType(ProgressTimer::Type::BAR);
	m_progressTime1->setMidpoint(Vec2(0.f, 0.f));
	m_progressTime1->runAction(progress1);


	auto act1 = DelayTime::create(3.f);
	auto act2 = CallFunc::create(CC_CALLBACK_0(LoadingScene::loadBegin, this));
	auto actResult = Sequence::create(act1, act2, NULL);
	this->runAction(actResult);

	auto TLabel = Label::createWithSystemFont("Loading...", "", 30.f);
	//auto TLabel = Label::createWithTTF("msyhbd.ttf", "Loading", TextHAlignment::CENTER, 300.f);
	TLabel->setPosition(centerPos.x, centerPos.y - 60.f);
	this->addChild(TLabel);




	creteOterProgress();
	return true;
}

bool LoadingScene::loadPic()
{
	log("loading...");
	auto TTextureCache = Director::getInstance()->getTextureCache();
	//TTextureCache->addImage(bire_all_res);
	TTextureCache->addImageAsync(bire_all_res, CC_CALLBACK_0(LoadingScene::loadOneRes, this));

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("FlappyBird/flappyBird.plist");
	//auto pic = Sprite::createWithTexture(TTextureCache->getTextureForKey(bire_all_res));
	//auto pic = Sprite::createWithSpriteFrameName("bird_bg.png");
	//pic->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	//this->addChild(pic);

	return true;
}

void LoadingScene::loadBegin()
{
	loadPic();
}

void LoadingScene::changeScene()
{
	log("change scene");
	unscheduleUpdate();
	auto scene = FlyBirdGame::createScene();
	//auto layer = MaskScene::create();
	//scene->addChild(layer);
	Director::getInstance()->replaceScene(scene);
}

Scene* LoadingScene::creteScene()
{
	auto scene = Scene::create();
	auto layer = LoadingScene::create();
	scene->addChild(layer);
	return scene;
}

void LoadingScene::creteOterProgress()
{
	Size visibSize = Director::getInstance()->getWinSize();
	Vec2 centerPos(visibSize.width / 2, visibSize.height*0.7f);
	auto progress2 = ProgressTo::create(3.f, 100.f);

	auto m_sprite1 = Sprite::create("FlappyBird/Loading2.png");
	auto progressTimmer2 = ProgressTimer::create(m_sprite1);
	progressTimmer2->setPosition(centerPos);
	this->addChild(progressTimmer2);

	//����
	progressTimmer2->setType(ProgressTimer::Type::RADIAL);
	progressTimmer2->runAction(progress2);

}

void LoadingScene::loadOneRes()
{
	++m_iLoadedResCount;

}

