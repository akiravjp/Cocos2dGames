#ifndef LOADING_SCENE_H_
#define LOADING_SCENE_H_
#include "../BaseDef.h"



class LoadingScene:public Layer
{
public:
	CREATE_FUNC(LoadingScene);
	virtual bool init();
	virtual void update(float dt);
	static Scene* creteScene();

	bool initUI();
	bool loadPic();
	void creteOterProgress();

	void loadBegin();
	void changeScene();

	void loadOneRes();

private:
	int m_iLoadedResCount;
};





#endif