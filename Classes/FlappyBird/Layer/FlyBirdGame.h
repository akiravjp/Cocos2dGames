#include"../BaseDef.h"


using namespace std;


class FlyBirdGame : cocos2d::Layer
{
public:
	FlyBirdGame();
	static cocos2d::Scene* createScene();
	CREATE_FUNC(FlyBirdGame);
private:
	virtual bool init();
	virtual void update(float dt);
	bool initUI();
	void gameStart(Object* pSender);

	void onTouchesBegin(const std::vector<Touch*>& touches, Event* event);
	void onTouchesEnded(const std::vector<Touch*>& touches, Event* event);
private:
	int score;

	float gravity;
	float velocity;

	EGameStateType m_GameState;
};

