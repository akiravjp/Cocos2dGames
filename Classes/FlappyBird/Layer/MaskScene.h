#ifndef MASK_SCENE_H_
#define MASK_SCENE_H_
#include "../BaseDef.h"

class MaskScene : public Layer
{
public:
	static Scene* Scene();
	CREATE_FUNC(MaskScene);
	virtual bool init();

private:
	void drawRectangularHelp();
	void drawCircularHelp();

	ClippingNode* clip;
};





#endif