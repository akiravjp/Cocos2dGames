#ifndef _WELCOM_LAYER_H_
#define _WELCOM_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"
#include "cocos/editor-support/cocostudio/CocoStudio.h"
#include "cocos/ui/CocosGUI.h"

class WelcomeLayer : public cocos2d::Layer
{
	typedef enum
	{
		BTN_TAG_START	=	999,
		BTN_TAG_SOUND
	}BtnTag;

public:
	WelcomeLayer();
	~WelcomeLayer();

public:
	static cocos2d::Scene* createScene();
public:
	virtual bool init();
	CREATE_FUNC(WelcomeLayer);
private:	
	void initCocosStudio();
	void menuOnTouch(cocos2d::Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
private:	
	cocos2d::ui::Widget *m_welcomeWid;

};

#endif	//_WELCOM_LAYER_H_


