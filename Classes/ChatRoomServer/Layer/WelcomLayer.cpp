#include "WelcomLayer.h"
#include "../Object/ChatRoomServer.h"

using namespace cocos2d;
using namespace ui;
using namespace cocostudio;

WelcomeLayer::WelcomeLayer() :	m_welcomeWid(nullptr)									
{

}
WelcomeLayer::~WelcomeLayer()
{

}

Scene* WelcomeLayer::createScene()
{
	auto sc		=	Scene::create();
	auto ly		=	WelcomeLayer::create();
	sc->addChild(ly);

	return sc;
}

bool WelcomeLayer::init()
{
	Layer::init();

	initCocosStudio();
	return true;
}

void WelcomeLayer::initCocosStudio()
{
	m_welcomeWid = GUIReader::getInstance()->widgetFromJsonFile("ChatRoomServer/ChatRoomServer_1/ChatRoomServer_1.ExportJson");
	this->addChild(m_welcomeWid);
	
	auto* btnStart = dynamic_cast<Button*>(m_welcomeWid->getChildByName("Button_2"));

	btnStart->addTouchEventListener(CC_CALLBACK_2(WelcomeLayer::menuOnTouch, this));

	return ;
}

void WelcomeLayer::menuOnTouch(Ref *pSender, Widget::TouchEventType type)
{
	std::string btnName = ((Node *)pSender)->getName();

	log("btnName  %s", btnName.c_str());
	
	switch(type)
	{
		case Widget::TouchEventType::BEGAN:
			log("btnTouchEvent  BEGAN");
			break;
		case Widget::TouchEventType::MOVED:
			log("btnTouchEvent  MOVED");
			break;
		case Widget::TouchEventType::ENDED:
			log("btnTouchEvent  ENDED");
			if (btnName == "Button_2")
			{
				CChatRoomServerApp::instance()->startServer();
			}

			break;
		case Widget::TouchEventType::CANCELED:
			log("btnTouchEvent  CANCELED");
			break;
		default:
			log("btnTouchEvent  default");
			break;

	}
	return ;
}



