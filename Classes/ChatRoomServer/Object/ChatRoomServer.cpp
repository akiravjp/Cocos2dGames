#include "ChatRoomServer.h"


using namespace cocos2d;
const int MAX_BUFFER_SIZE = 4096; 


static std::mutex g_insMutex;
CChatRoomServerApp* CChatRoomServerApp::_instance = NULL;

CChatRoomServerApp::CChatRoomServerApp():SERVER_PORT(8889),
												m_sockLister(INVALID_SOCKET)
{

	return ;
}

CChatRoomServerApp* CChatRoomServerApp::instance() 
{	
	if (NULL == _instance)
	{
		CGuard guard(g_insMutex);
		if (NULL == _instance)
		{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32		
			WSADATA wsa={0};
			WSAStartup(MAKEWORD(2,2),&wsa);
#endif
			_instance = new CChatRoomServerApp;
		}
	}
	return _instance;
}

bool CChatRoomServerApp::startServer()
{
	if(INVALID_SOCKET != m_sockLister )
	{
		MessageBox("server is runing!", "Title");
		return false;
	}
	
	// TODO: 在此添加控件通知处理程序代码


	//第一个参数：即协议域，又称为协议族（family）。常用的协议族有，AF_INET、AF_INET6、AF_LOCAL（或称AF_UNIX，Unix域socket）、AF_ROUTE等等。协议族决定了socket的地址类型，在通信中必须采用对应的地址，如AF_INET决定了要用ipv4地址（32位的）与端口号（16位的）的组合、AF_UNIX决定了要用一个绝对路径名作为地址。
	//第二个参数：指定socket类型。常用的socket类型有，SOCK_STREAM、SOCK_DGRAM、SOCK_RAW、SOCK_PACKET、SOCK_SEQPACKET等等。
	//第三个参数：故名思意，就是指定协议。常用的协议有，IPPROTO_TCP、IPPTOTO_UDP、IPPROTO_SCTP、IPPROTO_TIPC等，它们分别对应TCP传输协议、UDP传输协议、STCP传输协议、TIPC传输协议（这个协议我将会单独开篇讨论！）。
	m_sockLister = socket(AF_INET, SOCK_STREAM, 0);
	if(m_sockLister == INVALID_SOCKET)
	{
		MessageBox("create sock error!", "Title");
		int err = errno;
		return false;
	}

	int opt = 0;
	if (setsockopt(m_sockLister, SOL_SOCKET, SO_REUSEADDR, (char*) &opt, sizeof(opt))< 0)
	{
		MessageBox("set sock error!", "Title");
		return false;
	}
	struct sockaddr_in svraddr;
	svraddr.sin_family = AF_INET;
	svraddr.sin_addr.s_addr = INADDR_ANY;
	svraddr.sin_port = htons( SERVER_PORT );
	//s：标识一未捆绑套接口的描述字。
	//name：赋予套接口的地址。sockaddr结构定义如下：
	//namelen：name名字的长度。
	int ret = bind(m_sockLister, (struct sockaddr*) &svraddr, sizeof(svraddr));
	if (ret == SOCKET_ERROR)
	{
		MessageBox("bind sock error!", "Title");
		return false;
	}

	DWORD dwThreadID = 0;

	m_hListenThread = std::thread(&CChatRoomServerApp::ListenThread, this);
	m_hClientThread = std::thread(&CChatRoomServerApp::ClientThread, this);
	
	MessageBox("server is start!", "Title");
	return true;
}




void CChatRoomServerApp::ListenThread()
{
	//S：用于标识一个已捆绑未连接套接口的描述字。
	//backlog：等待连接队列的最大长度。
	int backlog = 5;
	int ret = listen(m_sockLister, backlog);
	if (ret == SOCKET_ERROR)
	{
		return;
	}

	while(1)
	{
		sockaddr_in clientAddr;
		int nLen = sizeof(sockaddr);
		//阻塞直到有客户端连接。
		SOCKET socket = accept(m_sockLister,(sockaddr*)&clientAddr,&nLen);
		
		{
			CGuard guard(m_clientReadMutex);
			m_listClientRead.push_back( socket );
		}
	}
	return;
}


void CChatRoomServerApp::ClientThread()
{	
	char recvBuf[MAX_BUFFER_SIZE];
	memset(recvBuf,0,MAX_BUFFER_SIZE);

	char sendBuf[MAX_BUFFER_SIZE];
	memset(sendBuf,0,MAX_BUFFER_SIZE);

	fd_set fd_read, fd_write;
	while( true )
	{
		auto listRead = m_listClientRead;
		auto listWirte = m_listClientWrite;
		FD_ZERO( & fd_read );
		FD_ZERO( & fd_write );
		for( auto a : listRead )
		{
			FD_SET( a, & fd_read );
		}
		for( auto a : listWirte )
		{
			FD_SET( a, & fd_write );
		}
		
		struct timeval cctv = {0, 50};
		int count = select( 100, & fd_read, & fd_write, NULL, & cctv );
		auto it1 = listRead.begin();
		while( count > 0 )
		{
			for( auto it2 : listWirte )
			{
				if( FD_ISSET( it2, & fd_write ) )
				{
					--count;
					Send( it2, sendBuf, strlen( sendBuf ) );
				}
			}
			m_listClientWrite.clear();
			if( FD_ISSET( * it1, & fd_read ) )
			{
				memset(recvBuf, 0, sizeof(recvBuf));
				if( 0 < Receive( * it1, recvBuf, sizeof( recvBuf ) ) )
				{
					strcpy( sendBuf, recvBuf );
					
					for( auto tmp : listRead )
					{
						m_listClientWrite.push_back( tmp );
					}
				}
				else
				{
					CGuard guard(m_clientReadMutex);
					for( auto tmp = m_listClientRead.begin(); tmp != m_listClientRead.end();  )
					{
						if( * it1 == * tmp )
						{
							closesocket( * tmp );
							tmp = m_listClientRead.erase( tmp );
						}
						else
						{
							++tmp;
						}
					}
				}
				break;
			}
			++it1;
		}
	}
}


int CChatRoomServerApp::Receive(SOCKET fd,char *szText,int len)
{
	int rc;
	//参数
	//一个标识已连接套接口的描述字。
	//用于接收数据的缓冲区。
	//缓冲区长度。
	//指定调用方式（MSG_PEEK）。
	//返回值
	//WSANOTINITIALISED：在使用此API之前应首先成功地调用WSAStartup()。
	//WSAENETDOWN：WINDOWS套接口实现检测到网络子系统失效。
	//WSAENOTCONN：套接口未连接。
	//WSAEINTR：阻塞进程被WSACancelBlockingCall()取消。
	//WSAEINPROGRESS：一个阻塞的WINDOWS套接口调用正在运行中。
	//WSAENOTSOCK：描述字不是一个套接口。
	//WSAEOPNOTSUPP：指定了MSG_OOB，但套接口不是SOCK_STREAM类型的。
	//WSAESHUTDOWN：套接口已被关闭。当一个套接口以0或2的how参数调用shutdown()关闭后，无法再用recv()接收数据。
	//WSAEWOULDBLOCK：套接口标识为非阻塞模式，但接收操作会产生阻塞。
	//WSAEMSGSIZE：数据报太大无法全部装入缓冲区，故被剪切。
	//WSAEINVAL：套接口未用bind()进行捆绑。
	//WSAECONNABORTED：由于超时或其他原因，虚电路失效。
	//WSAECONNRESET：远端强制中止了虚电路。
	rc=recv(fd,szText,len,0);
	if(rc <= 0)
	{
		return -1;
	}
	return rc;
}

int CChatRoomServerApp::Send(SOCKET fd,char *szText,int len)
{
	int cnt;
	int rc;
	cnt=len;
	while(cnt>0)
	{
		//参数
		//一个用于标识已连接套接口的描述字。
		//包含待发送数据的缓冲区。
		//缓冲区中数据的长度。
		//调用执行方式（MSG_DONTROUTE）。
		//返回值
		//WSANOTINITIALISED：在使用此API之前应首先成功地调用WSAStartup()。
		//WSAENETDOWN：WINDOWS套接口实现检测到网络子系统失效。
		//WSAEACESS：要求地址为广播地址，但相关标志未能正确设置。
		//WSAEINTR：通过一个WSACancelBlockingCall()来取消一个（阻塞的）调用。
		//WSAEINPROGRESS：一个阻塞的WINDOWS套接口调用正在运行中。
		//WSAEFAULT：buf参数不在用户地址空间中的有效位置。
		//WSAENETRESET：由于WINDOWS套接口实现放弃了连接，故该连接必需被复位。
		//WSAENOBUFS：WINDOWS套接口实现报告一个缓冲区死锁。
		//WSAENOTCONN：套接口未被连接。
		//WSAENOTSOCK：描述字不是一个套接口。
		//WSAEOPNOTSUPP：已设置了MSG_OOB，但套接口非SOCK_STREAM类型。
		//WSAESHUTDOWN：套接口已被关闭。一个套接口以1或2的how参数调用shutdown()关闭后，无法再用send()函数。
		//WSAEWOULDBLOCK：
		//WSAEMSGSIZE：套接口为SOCK_DGRAM类型，且数据报大于WINDOWS套接口实现所支持的最大值。
		//WSAEINVAL：套接口未用bind()捆绑。
		//WSAECONNABORTED：由于超时或其他原因引起虚电路的中断。
		//WSAECONNRESET：虚电路被远端复位。
		rc=send(fd,szText,cnt,0);
		if(rc==SOCKET_ERROR)
		{
			return -1;
		}
		if(rc==0)
		{
			return len-cnt;
		}
		szText+=rc;
		cnt-=rc;
	}
	return len;
}


