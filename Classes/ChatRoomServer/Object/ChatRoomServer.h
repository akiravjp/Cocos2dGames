#ifndef _CHAT_ROOT_SERVER_
#define _CHAT_ROOT_SERVER_
#include "Base/Base.h"


class CChatRoomServerApp
{
public:
	static CChatRoomServerApp* instance();
public:
	bool startServer();
private:
	CChatRoomServerApp();	
	void ListenThread();
	void ClientThread();
	int Receive(SOCKET fd,char *szText,int len);
	int Send(SOCKET fd,char *szText,int len);
private:
	const int SERVER_PORT;
	
	std::thread m_hListenThread;
	std::thread m_hClientThread;
	
	static  CChatRoomServerApp* _instance;
	SOCKET m_sockLister;

	std::list< SOCKET > m_listClientRead;
	std::list< SOCKET > m_listClientWrite;

	std::mutex m_clientReadMutex;
	std::mutex m_clienttWriteMutex;

};

#endif //_CHAT_ROOT_SERVER_

