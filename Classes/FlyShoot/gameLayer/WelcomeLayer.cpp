#include "WelcomeLayer.h"
#include "InGameLayer.h"

USING_NS_CC;

WelcomeLayer::WelcomeLayer(void) : logo(NULL)
{
}


WelcomeLayer::~WelcomeLayer(void)
{
}

Scene*	WelcomeLayer::createScene()
{
	Scene* sc			=	Scene::create();
	WelcomeLayer* ly	=	WelcomeLayer::create();
	sc->addChild(ly);

	return sc;
}

bool WelcomeLayer::init()
{
	Layer::init();

	Size winSize	=	Director::getInstance()->getVisibleSize();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("FlyShoot/gameArts.plist");
	
	Sprite* background		=	Sprite::createWithSpriteFrameName("background_2.png");
	this->addChild(background);
	background->setAnchorPoint(Point::ZERO);

	logo					=	Sprite::create("FlyShoot/BurstAircraftLogo.png");
	this->addChild(logo);
	logo->setPosition(winSize.width / 2,winSize.height - 100);

	Label* ttfStartGame	=	Label::createWithSystemFont("PLAY GMAE","WRYH",32); 
	MenuItemLabel* itemStart=	MenuItemLabel::create(ttfStartGame,CC_CALLBACK_1(WelcomeLayer::menuCallBack,this));

	Menu* startMenu			=	Menu::create(itemStart,NULL);
	this->addChild(startMenu);


	logoAction();

	return true;
}

void WelcomeLayer::logoAction()
{
	MoveBy* mMoveBy		=	MoveBy::create(1.0f,Point(0,-200));
	FadeIn* mFadeIn		=	FadeIn::create(2.0f);
	RotateTo* mRotateTo	=	RotateTo::create(3.0,180,36);
	logo->setOpacity(0);

	Spawn* mSpawn		=	Spawn::create(mFadeIn,mMoveBy,mRotateTo,NULL);//同时动画
	Sequence* seq		=	Sequence::create(mFadeIn,mMoveBy,mRotateTo,NULL);//连续动画
	logo->runAction(seq);
}

void WelcomeLayer::menuCallBack(cocos2d::Ref* sender)
{
	Director::getInstance()->replaceScene(TransitionPageTurn::create(0.5f,InGameLayer::createScene(),true));
}