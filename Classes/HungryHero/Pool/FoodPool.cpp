#include "HungryHero/Pool/FoodPool.h"


USING_NS_CC;


CFoodPool::CFoodPool(void):m_foodPoolList(), 
						MIN_COUNT(100), MAX_COUNT(200)
{
	initPool();
	return ;
}

CFoodPool::~CFoodPool(void)
{
}

void CFoodPool::initPool()
{
	CFood *pFood = NULL;
	for (int i=0; i<MIN_COUNT; ++i)
	{
		pFood = CFood::create();
		m_foodPoolList.pushBack(pFood);
	}
}


void CFoodPool::pushPool(CFood *pFood)
{
	if (m_foodPoolList.size() < MAX_COUNT)
	{
		m_foodPoolList.pushBack(pFood);
	}
	pFood->release();
	return ;
}


CFood *CFoodPool::popPool()
{
	CFood *pFood = NULL;
	if (m_foodPoolList.size() == 0)
	{
		pFood = CFood::create();
		m_foodPoolList.pushBack(pFood);
	}

	pFood = m_foodPoolList.at(m_foodPoolList.size()-1);
	pFood->retain();
	m_foodPoolList.eraseObject(pFood);

	return pFood;
}



