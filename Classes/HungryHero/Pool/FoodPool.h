#ifndef _FOOD_POOL_H_
#define _FOOD_POOL_H_

#include "cocos2d.h"
#include "Base/Base.h"

#include "HungryHero/Object/Food.h"

class CFoodPool
{
public:
	CFoodPool(void);
	~CFoodPool(void);
public:
	void pushPool(CFood *pFood);
	CFood *popPool();
		
private:
	void initPool();
	cocos2d::Vector<CFood *> m_foodPoolList;
	const int MIN_COUNT;
	const int MAX_COUNT;
};

#endif //_FOOD_POOL_H_
