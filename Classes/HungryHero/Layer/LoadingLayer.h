#ifndef _LOADING_LAYER_H_
#define _LOADING_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

class CLoadingLayer:public cocos2d::Layer
{
public:
	CLoadingLayer(void);
	~CLoadingLayer(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(CLoadingLayer);
	static cocos2d::Scene* createScence();
};

#endif	//_LOADING_LAYER_H_
