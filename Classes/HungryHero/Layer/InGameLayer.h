#ifndef _INGAME_LAYER_H_
#define _INGAME_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

#include "HungryHero/Object/Hero.h"
#include "HungryHero/Layer/BackgroundLayer.h"
#include "HungryHero/InGameState.h"
#include "HungryHero/Pool/FoodPool.h"

class InGameLayer:public cocos2d::Layer
{
public:
	InGameLayer(void);
	~InGameLayer(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(InGameLayer);
	static cocos2d::Scene* createScence();
	void gameStep(float dt);
private:
	void addHeroSprite();	
	void addPlayButton();	
	void addEvents();
	virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event);

	
	void moveByCircle();
	void moveByDregress();
	void moveUpDown();
	void rotationByMouse();

	CHero *m_hero;
	CBackgroundLayer *m_background;
	cocos2d::Point m_touchPoint;
	CFoodPool m_foodPool;
	cocos2d::Vector<CFood *> m_foodList;
	cocos2d::ParticleSystemQuad *m_eatParticle;
	float m_radiusAction;
	float m_dregress;
	float m_dist;
	//特殊能量的计数
	float m_mushrommPower;
	float m_cofferPower;
	float m_shakeWinPower;
	float m_shakeTimeGap;
	
	float m_heroSpeed;
	//最小速度
	const float MIN_HERO_SPEED;
	//最大速度
	const float MAX_HERO_SPEED;
public:
	enum 
	{
		e_modeCicle, 
		e_modeSin, 
		e_modeLine,
		e_modeSpecial, 
		e_modeNun, 
	}E_FOOD_CREATE_MODE;
	//创建某布局的距离记数
	float m_createFoodDist;
	//当前创建食物的模式
	int m_foodMode;
	enum
	{
		e_itemPlay,
		e_itemReplay,  
	};
	//-----------------------------------------------------
	//游戏状态机
	friend class IInGameState;
	InGameStateMachine m_inGameStateMachine;

	//食物状态机
	friend class IFoodState;
	friend class CFoodMachine;
	CFoodMachine m_foodMachine;

	//英雄状态机
	friend class IHeroState;
	friend class CHeroMachine;
	CHeroMachine m_heroMachine;
};

#endif	//_INGAME_LAYER_H_

