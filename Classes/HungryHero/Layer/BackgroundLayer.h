#ifndef _BACKGROUND_LAYER_H_
#define _BACKGROUND_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

#include "HungryHero/Object/Hero.h"
#include "HungryHero/Object/Background.h"

class CBackgroundLayer:public cocos2d::Layer
{
public:
	CBackgroundLayer(void);
	~CBackgroundLayer(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(CBackgroundLayer);
	void moveLeft();
	void setSpeed(float speed){m_speed = speed;};
	static cocos2d::Scene* createScence();
private:
#define BACKGROUND_SIZE 4
	CBackground *m_backgrounds[BACKGROUND_SIZE];
	float m_speed;
	
};

#endif	//_BACKGROUND_LAYER_H_
