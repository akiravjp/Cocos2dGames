#include "HungryHero/Layer/LoadingLayer.h"
#include "cocos/editor-support/cocostudio/CocoStudio.h"
#include "HungryHero/Layer/WelcomeLayer.h"
using namespace cocostudio;
USING_NS_CC;
CLoadingLayer::CLoadingLayer(void)
{
}


CLoadingLayer::~CLoadingLayer(void)
{
}

bool CLoadingLayer::init()
{
	Layer::init();
	return true;
}


Scene* CLoadingLayer::createScence()
{
	return WelcomeLayer::createScence();
}


