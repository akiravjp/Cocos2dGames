#include "HungryHero/Layer/WelcomeLayer.h"
#include "HungryHero/Layer/InGameLayer.h"


#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace CocosDenshion;

WelcomeLayer::WelcomeLayer(void):m_pLogo(NULL), m_hero(NULL)
{
}


WelcomeLayer::~WelcomeLayer(void)
{
}

void WelcomeLayer::testLabeText()
{
	Label *labelText = Label::createWithSystemFont("void WelcomeLayer::testLabelTTF()\n{	LabelTTF *ttf = LabelTTF::create();	return ;\n}", 
									"WRXY", 20, Size(100, 100));
	this->addChild(labelText);
	labelText->setPosition(VisibleRect::center());
	return ;
}

Scene* WelcomeLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = WelcomeLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool WelcomeLayer::init()
{
	Layer::init();

	Director::getInstance()->getWinSize();
	Size winSize = Director::getInstance()->getVisibleSize();

	 
		
	Sprite *background = Sprite::create("HungryHero/bgWelcome.jpg");
	this->addChild(background);
	background->setAnchorPoint(Point::ZERO);
	
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("HungryHero/hungryHero.plist");


	Sprite *btnStartNor = Sprite::createWithSpriteFrameName("startButton.png");
	Sprite *btnStartSel = Sprite::createWithSpriteFrameName("startButton.png");
	btnStartSel->setScale(0.8f);
	MenuItemSprite *itemStartSp = MenuItemSprite::create(btnStartNor, btnStartSel, NULL, CC_CALLBACK_1(WelcomeLayer::actionBtnCallBack,this));
	itemStartSp->setTag(e_itemStartSp);

	Sprite *btnAbouttNor = Sprite::createWithSpriteFrameName("welcome_aboutButton.png");
	Sprite *btnAboutSel = Sprite::createWithSpriteFrameName("welcome_aboutButton.png");
	btnAboutSel->setScale(0.8f);
	MenuItemSprite *itemAboutSp = MenuItemSprite::create(btnAbouttNor, btnAboutSel, NULL, CC_CALLBACK_1(WelcomeLayer::actionBtnCallBack,this));
	itemAboutSp->setTag(e_itemAboutSp);


	Menu *menus = Menu::create();
	this->addChild(menus);
	
	menus->addChild(itemStartSp);
	menus->addChild(itemAboutSp);
	menus->alignItemsVerticallyWithPadding(50);
	
	menus->setPosition(winSize.width-btnStartNor->getContentSize().width/2, 
						winSize.height/2-btnStartNor->getContentSize().height/2);
	
	

	MoveBy  *moveUp = MoveBy::create(1.5f, Vec2(0, 100));
	EaseBackInOut *easeUp = EaseBackInOut::create(moveUp);
	MoveBy  *moveDown = MoveBy::create(1.5f, Vec2(0, -100));
	EaseBackInOut *easeDown = EaseBackInOut::create(moveDown);

	//创建动作列表
	Sequence *seqMenus = Sequence::create(easeUp, easeDown, NULL);
	RepeatForever *repeatMenus = RepeatForever::create(seqMenus);

	menus->runAction(repeatMenus);


	m_pLogo = Sprite::createWithSpriteFrameName("welcome_title.png");
	this->addChild(m_pLogo);
	Vec2 logoPositio = VisibleRect::rightTop()-Vec2(m_pLogo->getContentSize().width/2, m_pLogo->getContentSize().height/2);
	m_pLogo->setPosition(logoPositio);


	m_hero = Sprite::createWithSpriteFrameName("welcome_hero.png");	
	this->addChild(m_hero);

	m_hero->setPosition(VisibleRect::left()-Vec2(m_hero->getContentSize().width/2, 0));

	Vec2 moveToWinPoint = Vec2(VisibleRect::getVisibleRect().getMaxX()/4, VisibleRect::getVisibleRect().getMaxY()/2);

	auto moveToHeroWin = MoveTo::create(1.0f, moveToWinPoint);
	auto moveByUpHeroFirst = MoveBy::create(1.0f, Vec2(0, 100));
	auto moveUpDownCallBack = CallFunc::create(CC_CALLBACK_0(WelcomeLayer::actionMoveUpDownCallBack, this));
	Sequence *seqHeroWinUp = Sequence::create(moveToHeroWin, moveByUpHeroFirst, moveUpDownCallBack, NULL);

	m_hero->runAction(seqHeroWinUp);

	addEvents();


	//testLabeText();
	soundToggle();
	return true;
}





void WelcomeLayer::soundToggle()
{
	//toggle menu
	auto spBtnSoundOpen = Sprite::createWithSpriteFrameName("soundOn0000.png");
	auto spBtnSoundOff = Sprite::createWithSpriteFrameName("soundOff.png");
	auto menuItemSoundOpen = MenuItemSprite::create(spBtnSoundOpen, NULL);
	auto menuItemSoundOff = MenuItemSprite::create(spBtnSoundOff, NULL);


	Vector<SpriteFrame *> btnSoundActionList;
	char soundSpriteFramePath[16];

	for (int i=0; i<3; ++i)
	{
		snprintf(soundSpriteFramePath, sizeof(soundSpriteFramePath), "soundOn%04d.png", i);
		auto soundSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(soundSpriteFramePath);

		btnSoundActionList.pushBack(soundSpriteFrame);
	}

	auto soundAnimation = Animation::createWithSpriteFrames(btnSoundActionList, 0.1f);
	auto soundAnimate = Animate::create(soundAnimation);
	spBtnSoundOpen->runAction(RepeatForever::create(soundAnimate));


	auto soundMenuItemToggle = MenuItemToggle::createWithCallback(CC_CALLBACK_1(WelcomeLayer::actionBtnCallBack, this), 
															menuItemSoundOpen, menuItemSoundOff, NULL);
	soundMenuItemToggle->setTag(e_itemSound);

	auto btnSoundMenu = Menu::create(soundMenuItemToggle, NULL);
	this->addChild(btnSoundMenu);

	btnSoundMenu->setPosition(VisibleRect::leftTop()+Vec2(50, -50));

	SimpleAudioEngine::getInstance()->playBackgroundMusic("HungryHero/sounds/bgWelcome.mp3");
	return ;
}



void WelcomeLayer::addEvents()
{
	log("addEvents !!!");
	auto touchEvt = EventListenerTouchOneByOne::create();
	touchEvt->onTouchBegan = CC_CALLBACK_2(WelcomeLayer::onTouchBegan, this);
	touchEvt->onTouchMoved = CC_CALLBACK_2(WelcomeLayer::onTouchMoved, this);
	touchEvt->onTouchEnded = CC_CALLBACK_2(WelcomeLayer::onTouchEnded, this);
	touchEvt->onTouchCancelled = CC_CALLBACK_2(WelcomeLayer::onTouchCancelled, this);

	//Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(touchEvt, 12);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchEvt, this);

	//三种移除方式
	//Director::getInstance()->getEventDispatcher()->removeEventListener(touchEvt);
	//Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(this);
	//Director::getInstance()->getEventDispatcher()->removeEventListenersForType(EventListener::Type::TOUCH_ONE_BY_ONE);
}

bool WelcomeLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchBegan !!!");
	//ture 后续的三个事件回调将会响应
	//false后续的三个事件回调将不再响应
	return true;
}

void WelcomeLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchMoved !!!");
	Vec2 curTouch = touch->getLocation();

	m_pLogo->setPosition(curTouch);
}

void WelcomeLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchEnded !!!");

}
void WelcomeLayer::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchCancelled !!!");

}





void WelcomeLayer::actionMoveUpDownCallBack()
{
	auto moveByHeroUp = MoveBy::create(1.0f, Vec2(0, 200));
	auto moveByHeroDown = MoveBy::create(1.0f, Vec2(0, -200));
	Sequence *seqHeroUpDown = Sequence::create(moveByHeroDown, moveByHeroUp, NULL);
	RepeatForever *repeatSeqHeroUpDown = RepeatForever::create(seqHeroUpDown);

	m_hero->runAction(repeatSeqHeroUpDown);

	return ;
}

void WelcomeLayer::actionBtnCallBack(Ref *target)
{
	auto menusItem = dynamic_cast<Node *>(target);

	int menusItemTag = menusItem->getTag();

	switch(menusItemTag)
	{
		case e_itemStartSp:
			gameStart();
			break;
		case e_itemAboutSp:
			gameAbout();
			break;
		case e_itemSound:
			gameSound();
			break;
		default:
			log("no menusItem  %d", menusItemTag);
			break;
	}

	return ;
}



void WelcomeLayer::gameStart()
{
	log("game start !!!");

	Scene *inGameLayerScene= InGameLayer::createScence();

	//replaceScene 在场景切换完之后， 将之前的场景销毁
	Director::getInstance()->replaceScene(TransitionProgressHorizontal::create(0.5f, inGameLayerScene));
	return ;
}

void WelcomeLayer::gameAbout()
{
	log("game about !!!");
	return ;
}

void WelcomeLayer::gameSound()
{
	bool isPlaying = SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying();
	if (isPlaying)
	{
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	}
	else
	{
		SimpleAudioEngine::getInstance()->playBackgroundMusic("HungryHero/sounds/bgWelcome.mp3");		
	}
	
	log("game sound isPlaying  %d!!!", isPlaying);
}

	

