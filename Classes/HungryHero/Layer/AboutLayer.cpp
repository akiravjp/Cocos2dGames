#include "HungryHero/Layer/AboutLayer.h"


USING_NS_CC;
AboutLayer::AboutLayer(void)
{
}


AboutLayer::~AboutLayer(void)
{
}

bool AboutLayer::init()
{
	Layer::init();
	return true;
}


Scene* AboutLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = AboutLayer::create();
	
	sc->addChild(ly);

	return sc;
}



