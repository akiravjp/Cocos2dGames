#ifndef _WELCOME_LAYER_H_
#define _WELCOME_LAYER_H_

#include "cocos2d.h"
#include "Base/Base.h"

class WelcomeLayer:public cocos2d::Layer
{
public:
	WelcomeLayer(void);
	~WelcomeLayer(void);
	virtual bool init();
	//create();
	CREATE_FUNC(WelcomeLayer);
	static cocos2d::Scene* createScence();
private:
	enum
	{
		e_itemStartSp,
		e_itemAboutSp, 
		e_itemSound, 
	};
	void addEvents();
	virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event);

	void actionMoveUpDownCallBack();
	void actionBtnCallBack(cocos2d::Ref *target);
	void soundToggle();
	void gameStart();
	void gameAbout();
	void gameSound();


	void testLabeText();

	cocos2d::Sprite *m_pLogo;
	cocos2d::Sprite *m_hero;
};

#endif	//_WELCOME_LAYER_H_
