#include "HungryHero/Layer/BackgroundLayer.h"


USING_NS_CC;
CBackgroundLayer::CBackgroundLayer(void):m_speed(1)
{
	for (int i=0; i<BACKGROUND_SIZE; ++i)
	{
		m_backgrounds[i] = NULL;
	}

	return ;
}


CBackgroundLayer::~CBackgroundLayer(void)
{
}




Scene* CBackgroundLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = CBackgroundLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool CBackgroundLayer::init()
{
	Layer::init();

	char fileName[32];

	for (int i=0; i<BACKGROUND_SIZE; ++i)
	{
		snprintf(fileName, sizeof(fileName), "bgLayer%d.png", i+1);
		StrMap fileNames;
		fileNames[0] = fileName;
		fileNames[1] = fileName;
		
		m_backgrounds[i] = CBackground::create(fileNames);
		CBackground *backgrounds = m_backgrounds[i];
		
		this->addChild(backgrounds);

		backgrounds->setAnchorPoint(Point::ZERO);
	}

	return true;
}

void CBackgroundLayer::moveLeft()
{
	for (int i=0; i<BACKGROUND_SIZE; ++i)
	{
		CBackground *background = m_backgrounds[i];
		background->moveLeft(m_speed*(i+1));
	}
	return ;
}


