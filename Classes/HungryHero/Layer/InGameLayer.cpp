#include "HungryHero/Layer/InGameLayer.h"


USING_NS_CC;
InGameLayer::InGameLayer(void):m_inGameStateMachine(this), 
							m_foodMachine(this), 
							m_heroMachine(this), 
							m_hero(NULL), m_radiusAction(0), 
							m_dregress(30), m_dist(10),
							m_foodPool(), m_foodList(), 
							m_createFoodDist(300), m_foodMode(e_modeSin), 
							MAX_HERO_SPEED(30.0f), MIN_HERO_SPEED(15.5f), m_heroSpeed(MIN_HERO_SPEED), 
							m_mushrommPower(0), m_cofferPower(0), m_shakeWinPower(0), 
							m_shakeTimeGap(10), 
							m_touchPoint(0, 0), 
							m_eatParticle(NULL)
							
							
{
}


InGameLayer::~InGameLayer(void)
{
}



Scene* InGameLayer::createScence()
{
	Scene* sc = Scene::create();

	auto ly = InGameLayer::create();
	
	sc->addChild(ly);

	return sc;
}

bool InGameLayer::init()
{
	Layer::init();

	m_background = CBackgroundLayer::create();
	this->addChild(m_background);

	m_background->setAnchorPoint(Point::ZERO);
	m_background->setSpeed(m_heroSpeed);

	addHeroSprite();
	addPlayButton();

	this->schedule(SEL_SCHEDULE(&InGameLayer::gameStep), 0.02f);
#if 0
	m_eatParticle = ParticleSystemQuad::create("eatParticle.plist");
	this->addChild(m_eatParticle);
	m_eatParticle->setPositionX(VisibleRect::getVisibleRect().getMaxX() + 100);
#endif
	addEvents();
	return true;
}
void InGameLayer::addHeroSprite()
{
	m_hero = CHero::create();
	this->addChild(m_hero);

	Point heroPoint = VisibleRect::left()-Point(VisibleRect::getVisibleRect().getMaxX()/4, 0);
	m_hero->setPosition(heroPoint);

}

void InGameLayer::addPlayButton()
{
	Sprite *btnPlayNor = Sprite::createWithSpriteFrameName("welcome_playButton.png");
	Sprite *btnPlaySel = Sprite::createWithSpriteFrameName("welcome_playButton.png");
	btnPlaySel->setScale(0.8f);

	//C++ lambda表达式
	//[=]时只有访问权限， [&]才有引发权限
	//[&m, &f], 指定lambda里面只能操作m和f
#if 0
	auto numFunc = [&](Ref *target)->int
					{
						m_gameState = e_stateReady;
						return 0;
					};
#endif
	auto menuItemCallBack = [&](Ref *target)
							{
								m_inGameStateMachine.clickPlayBtn();
							};
	MenuItemSprite *itemPlay = MenuItemSprite::create(btnPlayNor, btnPlaySel, NULL, menuItemCallBack);

	auto btnPlay = Menu::create(itemPlay, NULL);	
	this->addChild(btnPlay);

	btnPlay->setPosition(VisibleRect::center());
	btnPlay->setTag(e_itemPlay);

	return ;
}

void InGameLayer::addEvents()
{
	log("addEvents !!!");
	auto touchEvt = EventListenerTouchOneByOne::create();
	touchEvt->onTouchBegan = CC_CALLBACK_2(InGameLayer::onTouchBegan, this);
	touchEvt->onTouchMoved = CC_CALLBACK_2(InGameLayer::onTouchMoved, this);
	touchEvt->onTouchEnded = CC_CALLBACK_2(InGameLayer::onTouchEnded, this);
	touchEvt->onTouchCancelled = CC_CALLBACK_2(InGameLayer::onTouchCancelled, this);

	//Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(touchEvt, 12);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchEvt, this);

	//三种移除方式
	//Director::getInstance()->getEventDispatcher()->removeEventListener(touchEvt);
	//Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(this);
	//Director::getInstance()->getEventDispatcher()->removeEventListenersForType(EventListener::Type::TOUCH_ONE_BY_ONE);
	return ;
}


bool InGameLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchBegan !!!");

	//ture 后续的三个事件回调将会响应
	//false后续的三个事件回调将不再响应
	return true;
}

void InGameLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchMoved !!!");
	m_touchPoint = touch->getLocation();
	
}

void InGameLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchEnded !!!");

}
void InGameLayer::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
	log("Touch onTouchCancelled !!!");

}




//按圆形运行
void InGameLayer::moveByCircle()
{
	m_dregress += 10;

	float vx = cos(m_dregress*M_PI/180)*m_dist;
	float vy = sin(m_dregress*M_PI/180)*m_dist;

	m_hero->setPosition(m_hero->getPosition()+Point(vx, vy));
}

//按某一角度移动
void InGameLayer::moveByDregress()
{
	float vx = cos(m_dregress*M_PI/180)*m_dist;
	float vy = sin(m_dregress*M_PI/180)*m_dist;
	
	m_hero->setPosition(m_hero->getPosition()+Point(vx, vy));
}


//按sin值上下移动
void InGameLayer::moveUpDown()
{
	m_radiusAction += 0.1f;
	float y = sin(m_radiusAction)*10 + m_hero->getPositionY();

	m_hero->setPositionY(y);
}

//根据鼠标位置旋转角度
void InGameLayer::rotationByMouse()
{
	float dx = m_touchPoint.x - m_hero->getPositionX();
	float dy = m_touchPoint.y - m_hero->getPositionY();
	//弧度转角度CC_RADIANS_TO_DEGREES()
	//角度转弧度CC_DEGREES_TO_RADIANS()
	//m_dregress = atan(dy/dx)*180/M_PI;
	m_dregress = atan2(dy, dx)*180/M_PI;
	
	m_hero->setRotation(-m_dregress);

}

//时间步进器
void InGameLayer::gameStep(float dt)
{
	//按sin值上下移动
	//moveUpDown();
	//按某一角度移动
	//moveByDregress()
	//按圆形运行
	//moveByCircle();

	m_inGameStateMachine.runInGame();
	return ;
}











