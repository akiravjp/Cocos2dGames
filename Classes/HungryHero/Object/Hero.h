#ifndef _HERO_H_
#define _HERO_H_

#include "cocos2d.h"
#include "Base/Base.h"

#include "HungryHero/Object/Food.h"

class CHero: public cocos2d::Sprite
{
public:
	CHero(void);
	~CHero(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(CHero);

private:
	void initSkin();
};

//-------------------------------------------------------------

class InGameLayer;
class IHeroState;

typedef enum
{
	e_stateAlive, 
	e_stateDead,
	e_stateSpeeding, 
	e_stateHeroNum,	
}E_HERO_STATE;

class CHeroMachine:public IStateMachine<IHeroState>
{
public:
	CHeroMachine(InGameLayer *pInGameLayer);	
public:
	friend class IHeroState;	
	//动作列表
	void move(cocos2d::Point targetPoint);
	void hitFoodTest();
private:
	InGameLayer *m_inGameLayer;
};


class IHeroState 
{
protected:
	CHeroMachine *m_gameMachine;
	InGameLayer *&m_inGameLayer;
public:
	IHeroState(CHeroMachine *pMachine);
	virtual ~IHeroState(){}	
public:
	virtual void move(cocos2d::Point targetPoint){};
	virtual void hitFoodTest(){};
protected:
	CHero *&m_hero;
	cocos2d::Vector<CFood *> &m_foodList;
	CFoodPool &m_foodPool;
	float &m_cofferPower;
	float &m_mushrommPower;
	float &m_shakeWinPower;
	float &m_heroSpeed;
	//最小速度
	const float &MIN_HERO_SPEED;
	//最大速度
	const float &MAX_HERO_SPEED;
	
};

class CHeroAlive : public IHeroState
{
public:
	CHeroAlive(CHeroMachine *pMachine);
	void move(cocos2d::Point targetPoint);
	void hitFoodTest();
private:
	void NormalFoodAction();
	void cofferAction();
	void mushrommAction();
	void shakeWindows();
};
//-------------------------------------------------------------


#endif //_HERO_H_
