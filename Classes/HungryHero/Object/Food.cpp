#include "HungryHero/Object/Food.h"
#include "HungryHero/Pool/FoodPool.h"
#include "HungryHero/Layer/InGameLayer.h"

USING_NS_CC;


CFood::CFood(void):m_foodType(0)
{
	return ;
}

CFood::~CFood(void)
{
}


bool CFood::init()
{
	Sprite::init();

	return true;
}

void CFood::setFoodType(int foodType)
{
	char spriteFrameName[32];

	snprintf(spriteFrameName, sizeof(spriteFrameName), "item%d.png", foodType);
	initWithSpriteFrameName(spriteFrameName);

	return ;
}


int CFood::getFoodType()
{
	return m_foodType;
}

void CFood::setFoodSpeed(float foodSpeed)
{
	m_foodSpeed = foodSpeed;
}
float CFood::getFoodSpeed()
{
	return m_foodSpeed;
}

void CFood::moveFood()
{
	float x = this->getPositionX() - m_foodSpeed;
	this->setPositionX(x);

	//回收的代码
}


CFoodMachine::CFoodMachine(InGameLayer *pInGameLayer):	m_inGameLayer(pInGameLayer), 
															m_foodPool(m_inGameLayer->m_foodPool), 
															m_foodList(m_inGameLayer->m_foodList), 
															m_heroSpeed(m_inGameLayer->m_heroSpeed)
{
	insertMap(e_stateCicle, new CFoodCicle(this));
	insertMap(e_stateSin, new CFoodSin(this));
	insertMap(e_stateLine, new CFoodLine(this));
	insertMap(e_stateSpecial, new CFoodSpecial(this));
	return ;
}



void CFoodMachine::createFood()
{
	//这里是有问题的， foodState的值偶尔会为e_stateNun
	int foodState = (CCRANDOM_0_1() - 0.01) * e_stateFoodNun;
	setState((E_FOOD_STATE)foodState);
	m_state->createFood();	
	return ;
}

void CFoodMachine::moveAllFood()
{
	CFood *pFood = NULL;
	for (int i=m_foodList.size()-1; i>=0; --i)
	{
		pFood = m_foodList.at(i);
		pFood->setFoodSpeed(m_heroSpeed);
		pFood->moveFood();

		//回收至对象池
		if (pFood->getPositionX() <= -pFood->getContentSize().width)
		{
			m_inGameLayer->removeChild(pFood);
		
			m_foodList.eraseObject(pFood);
			m_foodPool.pushPool(pFood);
		}
	}
}



IFoodState::IFoodState(CFoodMachine *pGameMachine):	m_gameMachine(pGameMachine), 
														m_inGameLayer(m_gameMachine->m_inGameLayer), 
														m_foodPool(m_inGameLayer->m_foodPool), 
														m_heroSpeed(m_inGameLayer->m_heroSpeed), 
														m_foodList(m_inGameLayer->m_foodList), 
														m_createTimeGap(10), 
														CREATE_DIST_GAP(100)
{
}



CFoodCicle::CFoodCicle(CFoodMachine *pGameMachine):IFoodState(pGameMachine)
{
	return ;
}

void CFoodCicle::createFood()
{
	if (m_createTimeGap > 0)
	{
		--m_createTimeGap;
		return ;
	}
	else
	{
		m_createTimeGap = CREATE_DIST_GAP;
	}

	CFood *pFood = NULL;

	int createNum = CCRANDOM_0_1() * 60 + 20;
	int foodX = VisibleRect::right().x + 150;
	int foodY = CCRANDOM_0_1() * (VisibleRect::top().y -300) + 150;
	float angle = 30;
	int foodType;

	for (int i=0; i<createNum; ++i)
	{
		pFood = m_foodPool.popPool();
		m_inGameLayer->addChild(pFood);

		foodType = CCRANDOM_0_1() * 6 + 1;		
		pFood->setFoodType(foodType);
		pFood->setFoodSpeed(m_heroSpeed);


		float dx = cos(angle * M_PI / 180) * (100 + createNum - i);
		float dy = sin(angle * M_PI / 180) * (100 + createNum - i);
		angle += 30;
		Vec2 foodPoint = Vec2(foodX + dx, foodY + dy);
		pFood->setPosition(foodPoint);
		
	
		m_foodList.pushBack(pFood);
	}
	return ;
}


CFoodSin::CFoodSin(CFoodMachine *pGameMachine):IFoodState(pGameMachine)
{
	return ;
}

void CFoodSin::createFood()
{
	if (m_createTimeGap > 0)
	{
		--m_createTimeGap;
		return ;
	}
	else
	{
		m_createTimeGap = CREATE_DIST_GAP;
	}

	CFood *pFood = NULL;
	
	int foodX = CCRANDOM_0_1() * VisibleRect::getVisibleRect().getMaxX() + 50;
	int foodY = CCRANDOM_0_1() * VisibleRect::getVisibleRect().getMaxY();
	float radins = 0;
	int foodType;
	int createNum = CCRANDOM_0_1() * 50 + 20;
	
	for (int i=0; i<createNum; ++i)
	{
		pFood = m_foodPool.popPool();
		m_inGameLayer->addChild(pFood);
		
		foodType = CCRANDOM_0_1() * 6 + 1;		
		pFood->setFoodType(foodType);
		pFood->setFoodSpeed(m_heroSpeed);
		
		foodY += sin(radins) * CREATE_DIST_GAP;
		++radins;
		foodX += CREATE_DIST_GAP;

		Vec2 foodPoint = Vec2(foodX, foodY);
	
		pFood->setPosition(foodPoint);
		
		m_foodList.pushBack(pFood);
		
	}
	return ;
}


CFoodLine::CFoodLine(CFoodMachine *pGameMachine):IFoodState(pGameMachine)
{
	return ;
}

void CFoodLine::createFood()
{	
	if (m_createTimeGap > 0)
	{
		--m_createTimeGap;
		return ;
	}
	else
	{
		m_createTimeGap = CREATE_DIST_GAP;
	}

	int foodY = CCRANDOM_0_1() * VisibleRect::getVisibleRect().getMaxY() - 50;
	

	int foodType;
	CFood *pFood = NULL;
	int createNum = CCRANDOM_0_1() * 10 + 5;

	for (int i=0; i<createNum; ++i)
	{
		pFood = m_foodPool.popPool();
		m_inGameLayer->addChild(pFood);

		foodType = CCRANDOM_0_1() * 6 + 1;		
		pFood->setFoodType(foodType);
		pFood->setFoodSpeed(m_heroSpeed);

		int foodX = VisibleRect::getVisibleRect().getMaxX() + pFood->getContentSize().width + i*CREATE_DIST_GAP;
		Vec2 foodPoint = Vec2(foodX, foodY);
		
		pFood->setPosition(foodPoint);

		m_foodList.pushBack(pFood);
	}

	return ;
}

CFoodSpecial::CFoodSpecial(CFoodMachine *pGameMachine):IFoodState(pGameMachine)
{
	return ;
}

void CFoodSpecial::createFood()
{
	if (m_createTimeGap > 0)
	{
		--m_createTimeGap;
		return ;
	}
	else
	{
		m_createTimeGap = CREATE_DIST_GAP;
	}

	CFood *pFood = NULL;

	m_createTimeGap = 50;
	pFood = m_foodPool.popPool();
	m_inGameLayer->addChild(pFood);

	int foodType = CCRANDOM_0_1() * 2 + 5;	
	pFood->setFoodType(foodType);
	pFood->setFoodSpeed(m_heroSpeed);


	int foodX = VisibleRect::right().x + 50;
	int foodY = CCRANDOM_0_1() * (VisibleRect::top().y - 50) + 25;
	Vec2 foodPoint = Vec2(foodX, foodY);
	
	pFood->setPosition(foodPoint);
	

	m_foodList.pushBack(pFood); 
}





