#ifndef _FOOD_H_
#define _FOOD_H_

#include "cocos2d.h"
#include "Base/Base.h"
class CFoodPool;




class CFood: public cocos2d::Sprite
{
public:
	CFood(void);
	~CFood(void);
public:
	virtual bool init();
	//create();
	CREATE_FUNC(CFood);
	void setFoodType(int foodType);
	int getFoodType();
	void setFoodSpeed(float foodSpeed);
	float getFoodSpeed();
	void moveFood();
private:
	int m_foodType;
	float m_foodSpeed;
};


//---------------------------------------------------------------


class InGameLayer;
class CFoodMachine;
typedef enum 
{
	e_stateCicle, 
	e_stateSin, 
	e_stateLine,
	e_stateSpecial, 
	e_stateFoodNun
}E_FOOD_STATE;



class IFoodState 
{
protected:
	CFoodMachine *m_gameMachine;
	InGameLayer *&m_inGameLayer;
public:
	IFoodState(CFoodMachine *pGameMachine);
	virtual ~IFoodState(){}
public:
	virtual void createFood() = 0;
protected:
	CFoodPool &m_foodPool;
	float &m_heroSpeed;
	cocos2d::Vector<CFood *> &m_foodList;

	//定时创建一组食物的时间记数
	float m_createTimeGap;	
	//食物布局间隔
	const int CREATE_DIST_GAP;
	
};



class CFoodCicle : public IFoodState
{
public:
	CFoodCicle(CFoodMachine *pGameMachine);
	void createFood();
};



class CFoodSin : public IFoodState
{
public:
	CFoodSin(CFoodMachine *pGameMachine);
	void createFood();
};

class CFoodLine : public IFoodState
{
public:
	CFoodLine(CFoodMachine *pGameMachine);
	void createFood();
};


class CFoodSpecial : public IFoodState
{
public:
	CFoodSpecial(CFoodMachine *pGameMachine);
	void createFood();
};


class CFoodMachine:public IStateMachine<IFoodState>
{
private:	
	friend class IFoodState;
	//标明该状态机属于谁
	InGameLayer *m_inGameLayer;
public:
	CFoodMachine(InGameLayer *pInGameLayer);
private:
	void createStateMachine();
	void destroyStateMachine();

public:		
	//动作列表
	void createFood();
	void moveAllFood();
private:	
	CFoodPool &m_foodPool;
	cocos2d::Vector<CFood *> &m_foodList;
	float &m_heroSpeed;
};

#endif //_HERO_H_
