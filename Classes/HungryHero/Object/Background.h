#ifndef _BACKGROUN_H_
#define _BACKGROUN_H_

#include "cocos2d.h"
#include "Base/Base.h"


class CBackground:public IBackground
{
public:
	CREATE_BACKGROUND(CBackground);	
private:
	Sprite *getBackground(const std::string &file) override;
};

#endif //_BACKGROUN_H_


