#include "HungryHero/Object/Hero.h"
#include "HungryHero/Layer/InGameLayer.h"

USING_NS_CC;


CHero::CHero(void)
{
	return ;
}

CHero::~CHero(void)
{
}


bool CHero::init()
{
	Sprite::init();


	initSkin();
	return true;
}

void CHero::initSkin()
{
	
	Vector<SpriteFrame *> heroActionList;
	SpriteFrame* heroFrame = NULL;
	char heroFramePathKey[64];

	for (int i=0;i<20; ++i)
	{
		snprintf(heroFramePathKey, sizeof(heroFramePathKey), "fly_%04d.png", i+1);
		heroFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(heroFramePathKey);
		heroActionList.pushBack(heroFrame);
	}

	//脚本
	auto *animation = Animation::createWithSpriteFrames(heroActionList, 0.2f);
	//拍好电影的胶片
	auto *animate = Animate::create(animation);
	
	this->runAction(RepeatForever::create(animate));	
	
}


//-------------------------------------------------------------


CHeroMachine::CHeroMachine(InGameLayer *pInGameLayer):m_inGameLayer(pInGameLayer)
{		
	insertMap(e_stateAlive, new CHeroAlive(this));
}


void CHeroMachine::move(cocos2d::Point targetPoint)
{
	m_state->move(targetPoint);
	return ;
}

void CHeroMachine::hitFoodTest()
{
	m_state->hitFoodTest();;
}

IHeroState::IHeroState(CHeroMachine *pMachine): 	m_gameMachine(pMachine), 
													m_inGameLayer(m_gameMachine->m_inGameLayer), 
													m_hero(m_inGameLayer->m_hero), 
													m_foodList(m_inGameLayer->m_foodList), 
													m_foodPool(m_inGameLayer->m_foodPool), 
													m_cofferPower(m_inGameLayer->m_cofferPower), 
													m_mushrommPower(m_inGameLayer->m_mushrommPower), 
													m_heroSpeed(m_inGameLayer->m_heroSpeed),
													MAX_HERO_SPEED(m_inGameLayer->MAX_HERO_SPEED),
													MIN_HERO_SPEED(m_inGameLayer->MIN_HERO_SPEED),
													m_shakeWinPower(m_inGameLayer->m_shakeWinPower)
{
	
	return ;
}

//----------------------------------------------------------------------------

CHeroAlive::CHeroAlive(CHeroMachine *pMachine):IHeroState(pMachine)
{

}


void CHeroAlive::move(cocos2d::Point targetPoint)
{
	//设置英雄速度
	if (m_cofferPower > 0)
	{
		m_heroSpeed = MAX_HERO_SPEED;
		--m_cofferPower;
	}
	else
	{
		m_cofferPower = 0;
		m_heroSpeed = MIN_HERO_SPEED;
	}

	float dy = targetPoint.y - m_hero->getPositionY();
	m_hero->setPositionY(m_hero->getPositionY() + dy/10);

	//角度计算
	float dx = targetPoint.x - m_hero->getPositionX();
	float radins = -atan2(dy, dx);
	float angle = radins * 180 / M_PI;

	if (angle > 30)
	{
		angle = 30;
	}
	else if (angle < -30)
	{
		angle = -30;
	}
	m_hero->setRotation(angle);
	return ;
}



void CHeroAlive::hitFoodTest()
{

	CFood *pFood = NULL;
	float dist = 0;
	float dx = 0;
	float dy = 0;
	for (int i=m_foodList.size()-1; i>=0; --i)
	{
		pFood = m_foodList.at(i);
		dx = m_hero->getPositionX()- pFood->getPositionX();
		dy = m_hero->getPositionY()- pFood->getPositionY();
		dist = sqrt(dx*dx + dy*dy);
		if (dist <= (40 + pFood->getContentSize().width / 2))
		{
			if (pFood->getFoodType() < 6)
			{
				NormalFoodAction();
			}
			else if (pFood->getFoodType() == 6)
			{
				cofferAction();
			}
			else if (pFood->getFoodType() == 7)
			{
				mushrommAction();
			}
			m_inGameLayer->removeChild(pFood);
			m_foodList.eraseObject(pFood);
			m_foodPool.pushPool(pFood);


		}
	}
}
void CHeroAlive::NormalFoodAction()
{
	//粒子效果
	//eatParticle结点一直存在，会造成内存泄漏
	//可以将粒子效果提升到数据成员， 重复使用 
	/*
	m_eatParticle->setPosition(pFood->getPosition());
	m_eatParticle->setLife(-1);
	
	//另一种方案
	ParticleSystemQuad *eatParticle = ParticleSystemQuad::create("eatParticle.plist");
	m_inGameLayer->addChild(eatParticle);
	
	eatParticle->setPosition(pFood->getPosition());
	eatParticle ->setAutoRemoveOnFinish(true);
	*/

}

void CHeroAlive::cofferAction()
{
	m_cofferPower += 10; 
}

void CHeroAlive::mushrommAction()
{
	m_mushrommPower += 10;
	m_shakeWinPower += 3;
}

void CHeroAlive::shakeWindows()
{
	if (m_shakeWinPower > 0)
	{
		m_shakeWinPower -= 0.3f;
		m_inGameLayer->setPositionX(CCRANDOM_0_1() * 10 + 3);
		m_inGameLayer->setPositionY(CCRANDOM_0_1() * 10 + 3);
		m_inGameLayer->setRotation(CCRANDOM_0_1() * 5 - 2.5f);
	}
	else
	{
		m_inGameLayer->setPosition(Point::ZERO);
	}
}



