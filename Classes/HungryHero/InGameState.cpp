#include "Layer/InGameLayer.h"
#include "Layer/BackgroundLayer.h"

#include "Object/Hero.h"
#include "Object/Food.h"

//-----------------------------------------------
#include "InGameState.h"
USING_NS_CC;

InGameStateMachine::InGameStateMachine(InGameLayer *pInGameLayer):m_inGameLayer(pInGameLayer)
{
	insertMap(e_stateWait, new CGameWait(this));
	insertMap(e_stateReady, new CGameReady(this));
	insertMap(e_statePlay, new CGamePlay(this));
	insertMap(e_stateOver, new CGameOver(this));
}

//-----------------------------------------------
void InGameStateMachine::runInGame()
{
	return m_state->runInGame();
}

void InGameStateMachine::clickPlayBtn()
{
	return m_state->clickPlayBtn();
}




IInGameState::IInGameState(InGameStateMachine *pGameMachine):
														m_gameMachine(pGameMachine), 
														m_inGameLayer(m_gameMachine->m_inGameLayer), 
														m_hero(m_inGameLayer->m_hero), 
														m_background(m_inGameLayer->m_background), 
														m_touchPoint(m_inGameLayer->m_touchPoint), 
														m_foodPool(m_inGameLayer->m_foodPool), 
														m_foodList(m_inGameLayer->m_foodList),  
														m_heroSpeed(m_inGameLayer->m_heroSpeed),
														m_cofferPower(m_inGameLayer->m_cofferPower),
														m_mushrommPower(m_inGameLayer->m_mushrommPower),
														m_shakeWinPower(m_inGameLayer->m_shakeWinPower),
														m_shakeTimeGap(m_inGameLayer->m_shakeTimeGap),
														m_eatParticle(m_inGameLayer->m_eatParticle), 
														MIN_HERO_SPEED(m_inGameLayer->MIN_HERO_SPEED), 
														MAX_HERO_SPEED(m_inGameLayer->MAX_HERO_SPEED), 
														m_createFoodDist(m_inGameLayer->m_createFoodDist), 
														m_foodMode(m_inGameLayer->m_foodMode), 
														m_foodMachine(m_inGameLayer->m_foodMachine), 
														m_heroMachine(m_inGameLayer->m_heroMachine)
														
{
}

void IInGameState::setState(E_STATE machineState)
{
	return m_gameMachine->setState(machineState);
}


void CGameWait::clickPlayBtn()
{
	setState(e_stateReady);
}


void CGameReady::runInGame()
{	
	float dx = VisibleRect::getVisibleRect().getMaxX()/3-m_hero->getPositionX();

	float vx = dx/10;
	m_hero->setPositionX(m_hero->getPositionX()+vx);

	if (dx < 5)
	{
		m_hero->setPositionX(VisibleRect::getVisibleRect().getMaxX()/3);
		auto menuItemPlay = m_inGameLayer->getChildByTag(InGameLayer::e_itemPlay);
		menuItemPlay->setVisible(false);

		setState(e_statePlay);
	}
	return;
}


void CGamePlay::runInGame()
{
	m_heroMachine.move(m_touchPoint);	
	m_background->setSpeed(m_heroSpeed);
	m_background->moveLeft();

	m_foodMachine.createFood();
	m_foodMachine.moveAllFood();
	m_heroMachine.hitFoodTest();
	return ;
}



void CGameOver::runInGame()
{	
	m_hero->removeFromParent();
	
	//this->unscheduleAllSelectors();
	m_inGameLayer->unschedule(SEL_SCHEDULE(&InGameLayer::gameStep));
}

