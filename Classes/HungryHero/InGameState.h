#ifndef _IN_GAME_STATE_H_
#define _IN_GAME_STATE_H_


#include <cassert>
#include "cocos2d.h"
#include "Base/Base.h"


#include "Object/Hero.h"
#include "Layer/BackgroundLayer.h"
#include "Pool/FoodPool.h"

class InGameLayer;
class IInGameState;
class CGameWait;
class CGameReady;
class CGamePlay;
class CGameOver;


typedef enum
{
	e_stateWait, 
	e_stateReady,
	e_statePlay, 
	e_stateOver,	
}E_STATE;

class InGameStateMachine:public IStateMachine<IInGameState>
{
public:
	InGameStateMachine(InGameLayer *pInGameLayer);

private:	
	friend class IInGameState;
	//标明该状态机属于谁
	InGameLayer *m_inGameLayer;
public:	
	//动作列表
	void runInGame();
	void clickPlayBtn();
};


class IInGameState 
{
protected:
	InGameStateMachine *m_gameMachine;
	InGameLayer *&m_inGameLayer;
public:
	IInGameState(InGameStateMachine *pGameMachine);
	virtual ~IInGameState(){}
	//状态机的操作列表
	virtual void setState(E_STATE machineState);
	
public:
	virtual void clickPlayBtn(){}
	virtual void runInGame(){}
	virtual void changeFoodCreateMode(){}
	virtual void moveBackground(){}
protected:
	CHero *&m_hero;
	CBackgroundLayer *&m_background;
	cocos2d::Point &m_touchPoint;
	CFoodPool &m_foodPool;
	cocos2d::Vector<CFood *> &m_foodList;
	float &m_heroSpeed;
	float &m_cofferPower;		
	float &m_mushrommPower;
	float &m_shakeWinPower;
	float &m_shakeTimeGap;
	cocos2d::ParticleSystemQuad *&m_eatParticle;
	//最小速度
	const float &MIN_HERO_SPEED;
	//最大速度
	const float &MAX_HERO_SPEED;	
	float &m_createFoodDist;
	//当前创建食物的模式
	int &m_foodMode;
	//食物状态机
	CFoodMachine &m_foodMachine;
	//英雄状态机
	CHeroMachine &m_heroMachine;
};
//-----------------------------------------------

class CGameWait : public IInGameState
{
public:
	CGameWait(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
	void clickPlayBtn();
	void heroDied();
};

class CGameReady : public IInGameState
{
public:
	CGameReady(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
	void runInGame();
};
class CGamePlay : public IInGameState
{
public:
	CGamePlay(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
	void runInGame();
	void hitFoodTest();
	void NormalFoodAction();
	void cofferAction();
	void mushrommAction();
	void shakeWindows();
	void moveAllFood();
};
class CGameOver : public IInGameState
{
public:
	CGameOver(InGameStateMachine *pGameMachine):IInGameState(pGameMachine){}
	void runInGame();
};

#endif



