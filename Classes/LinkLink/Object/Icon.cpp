#include "Icon.h"
#include "LinkLink/GameConst.h"

using namespace std;
using namespace cocos2d;
Icon::Icon() : m_type(0), 
				m_access(false)
{
}


Icon::~Icon()
{
}

bool Icon::init()
{
	Sprite::init();

	return true;
}

void Icon::setType(int type)
{
	char chType[100];
	sprintf(chType, "icon_%d.png", type);
	initWithSpriteFrameName(chType);

	this->m_type = type;

}


int Icon::getType() const
{
	return m_type;
}

void Icon::setMapPosition(int row, int col)
{
	this->m_row = row;
	this->m_col = col;
	
	this->setPositionX(col * ICON_GAP);
	this->setPositionY(row * ICON_GAP);

}

int  Icon::getRow() const
{
	return m_row;
}
int  Icon::getCol() const
{
	return m_col;
}

void Icon::setAccess(bool access)
{
	m_access = access;	
	if(m_access)
	{
		this->setColor(Color3B(255,99,00));			
		this->setVisible(false);
	}
}
bool Icon::isAccess()
{
	return m_access;
}


void Icon::selectedAction(bool isSelect)
{
	if (isSelect)
	{
		auto fade	=	FadeTo::create(0.1f,255);
		auto out	=	FadeTo::create(0.1f,100);
		auto seq	=	Sequence::create(fade,out,NULL);
		auto repeat	=	RepeatForever::create(seq);
		this->runAction(repeat);
	}else
	{
		this->stopAllActions();
		this->setOpacity(255);
	}
}


