#ifndef __ICONS_MAPS_H_
#define __ICONS_MAPS_H_

#include <map>
#include <vector>
#include "cocos2d.h"
#include "Base/Base.h"
#include "../Object/Icon.h"


class CIconsMap : public cocos2d::Sprite
{
public:
	CIconsMap();
public:
	CREATE_FUNC(CIconsMap)
	bool init() override;
	void moveToCenter();
	void touchMap(cocos2d::Point &touchPos);	
	int getRow();
	int getCol();
private:
	void initIconsMap();	
	void initBatchNode();
	void createMap(int mapRow, int mapCol);
	Icon *createIcon(int row, int col, int iconType);
	void disorderMap();
	void disorderOnce();
	Icon *touchWitchIcon(cocos2d::Point &localPos);
	void createEdge();
	void setEdgeIcon(int row, int col);
	void resetTouchIcons();
	bool isConnect();
	bool isLineAccess(Icon *firstTouchIcon, Icon *secondTouchIcon);
	typedef std::function<bool(int )> IconAccessCallback;
	bool isIconAccess(int first, int second, IconAccessCallback callback);

	bool isOneCornerAccess(Icon *firstTouchIcon, Icon *secondTouchIcon);
	bool isTwoCornerAccess(Icon *firstTouchIcon, Icon *secondTouchIcon);
	void dealTwoIcon();
	
private:
	typedef std::map<int, std::map<int, Icon*>> IconsMap;	
	IconsMap m_iconsMap;

	cocos2d::SpriteBatchNode* m_iconBatchNode;
	int m_mapRow;
	int m_mapCol;

	Icon *m_firstTouchIcon;
	Icon *m_secondTouchIcon;
	std::vector<cocos2d::Point> m_points;
};

#endif  //__ICONS_MAPS_H_


