#ifndef _ICON_H_
#define _ICON_H_

#include "cocos2d.h"


class Icon : public cocos2d::Sprite
{
public:
	Icon();
	~Icon();

public:

	virtual bool init();
	CREATE_FUNC(Icon);

public:

	void setType(int type);
	int getType() const;

	void setMapPosition(int row,int col);
	int  getRow() const;
	int	 getCol() const;
	void setAccess(bool access);
	bool isAccess();
	void selectedAction(bool isSelect);

private:

	int m_type;

	int m_row;
	int m_col;
	bool m_access;
};

#endif	//_ICON_H_

