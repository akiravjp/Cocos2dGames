#include <vector>
#include "IconsMap.h"
#include "../GameConst.h"
#include "../Layer/DrawLayer.h"

using namespace cocos2d;


CIconsMap::CIconsMap():	m_mapRow(10), 
							m_mapCol(10), 
							m_iconBatchNode(NULL), 
							m_firstTouchIcon(NULL), 
							m_secondTouchIcon(NULL)
{

}

bool CIconsMap::init()
{
	Sprite::init();
	initIconsMap();
	initBatchNode();
	createMap(m_mapRow, m_mapCol);
	disorderMap();
	return true;
}
int CIconsMap::getRow()
{
	return m_mapRow;
}
int CIconsMap::getCol()
{
	return m_mapCol;
}

void CIconsMap::initIconsMap()
{
	this->setContentSize(Size(ICON_GAP*m_mapRow, ICON_GAP*m_mapCol));
	this->setAnchorPoint(Point::ZERO);
}

void CIconsMap::initBatchNode()
{
	m_iconBatchNode = SpriteBatchNode::create("LinkLink/icon.png", 50);
	this->addChild(m_iconBatchNode);
	
	m_iconBatchNode->setAnchorPoint(Point::ZERO);
	return ;
}


void CIconsMap::createMap(int mapRow, int mapCol)
{
	Icon* icon = NULL;
	int iconNum = 0;
	int iconType = 0;
	for (int row = 1; row < mapRow-1; row++)
	{
		for (int col = 1; col < mapCol-1; col++)
		{
			if (++iconNum % 2)
			{
				iconNum %= 2;
				iconType = CCRANDOM_0_1() * 15 + 1;
			}

			icon = createIcon(row, col, iconType);
			
			m_iconsMap[row][col] = icon;
			m_iconBatchNode->addChild(icon);
		}
	}
	
	createEdge();

}

Icon *CIconsMap::createIcon(int row, int col, int iconType)
{
	Icon *icon = NULL;
	icon = Icon::create(); 		
	icon->setType(iconType);
	icon->setMapPosition(row, col);
	icon->setAnchorPoint(Point::ZERO);
	return icon;
}

void CIconsMap::disorderMap()
{
	int forSize = m_mapRow * m_mapCol;
	for (int i=0; i<forSize; ++i)
	{
		disorderOnce();
	}	
	return ;
}

void CIconsMap::disorderOnce()
{
	int firstRow = CCRANDOM_0_1() * (m_mapRow - 2) + 1;
	int firstCol = CCRANDOM_0_1() * (m_mapCol - 2) + 1;

	int secondRow = CCRANDOM_0_1() * (m_mapRow - 2) + 1;
	int secondCol = CCRANDOM_0_1() * (m_mapCol - 2) + 1;

	Icon *firstIcon = m_iconsMap[firstRow][firstCol];
	Icon *secondIcon = m_iconsMap[secondRow][secondCol];
	
	firstIcon->setMapPosition(secondRow, secondCol);
	secondIcon->setMapPosition(firstRow, firstCol);

	m_iconsMap[firstRow][firstCol] = secondIcon;
	m_iconsMap[secondRow][secondCol] = firstIcon;
}

void CIconsMap::createEdge()
{
	Icon* icon = NULL;

	int col = 0;
	
	for (int row=0; row<m_mapRow; ++row)
	{
		col = 0;
		setEdgeIcon(row, col);
		col = m_mapCol-1;
		setEdgeIcon(row, col);
	}

	int row = 0;
	for (int col=0; col<m_mapCol; ++col)
	{
		row = 0;
		setEdgeIcon(row, col);
		row = m_mapRow-1;
		setEdgeIcon(row, col);
	}
	return ;
}

void CIconsMap::setEdgeIcon(int row, int col)
{
	Icon* icon = NULL;
	icon = createIcon(row, col, 1);
	m_iconBatchNode->addChild(icon);
	
	m_iconsMap[row][col] = icon;
	icon->setAccess(true);
	return ;
}

void CIconsMap::moveToCenter()
{	
	Point toPos = VisibleRect::center() - Point(ICON_GAP*m_mapRow/2, ICON_GAP*m_mapCol/2);
	this->setPosition(toPos);
	return ;
}


void CIconsMap::touchMap(Point &touchPos)
{
	Point localPos	=	m_iconBatchNode->convertToNodeSpace(touchPos);

	Icon *icon = touchWitchIcon(localPos);
	if (!icon || icon->isAccess())
	{
		return ;
	}
	if (!m_firstTouchIcon)
	{
		m_firstTouchIcon = icon;
		m_firstTouchIcon->selectedAction(true);
	}
	else if (!m_secondTouchIcon)
	{
		m_secondTouchIcon = icon;		
		m_secondTouchIcon->selectedAction(true);
	}

	if (m_firstTouchIcon && m_secondTouchIcon)
	{
		dealTwoIcon();
		resetTouchIcons();

		
	}

	return ;
}
void CIconsMap::resetTouchIcons()
{
	m_firstTouchIcon->selectedAction(false);
	m_secondTouchIcon->selectedAction(false);
	m_firstTouchIcon = NULL;
	m_secondTouchIcon = NULL;
}

Icon *CIconsMap::touchWitchIcon(Point &localPos)
{
	Icon *icon = NULL;
	for (int row=0; row<m_mapRow; ++row)
	{
		for (int col=0; col<m_mapCol; ++col)
		{
			icon = m_iconsMap[row][col];
			Rect iconRect = icon->getBoundingBox();
			bool touched = iconRect.containsPoint(localPos);
			if (touched)
			{
				log("row, col  %d  %d", row, col);
				return icon;
			}
		}
	}
	return NULL;
}

void CIconsMap::dealTwoIcon()
{
	if (!m_firstTouchIcon || !m_secondTouchIcon)
	{
		return;
	}

	if (m_firstTouchIcon != m_secondTouchIcon
		&& (m_firstTouchIcon->getType() == m_secondTouchIcon->getType())
		&& isConnect() 
		)
	{
	
		auto drawLayer = CDrawLayer::create();
		this->addChild(drawLayer);
		drawLayer->initDraw(m_mapRow, m_mapCol);
		
		m_firstTouchIcon->setAccess(true);
		m_secondTouchIcon->setAccess(true);
		
	}
}

bool CIconsMap::isConnect()
{
	if (!m_firstTouchIcon || !m_secondTouchIcon)
	{
		return false;
	}
	m_points.clear();
	m_points.push_back(m_secondTouchIcon->getPosition());
	if (isLineAccess(m_firstTouchIcon, m_secondTouchIcon)
		|| isOneCornerAccess(m_firstTouchIcon, m_secondTouchIcon)
		|| isTwoCornerAccess(m_firstTouchIcon, m_secondTouchIcon)
		)
	{
		m_points.push_back(m_firstTouchIcon->getPosition());
		CDrawLayer::setDrawPoints(m_points);	
		return true;
	}
	return false;
}

bool CIconsMap::isLineAccess(Icon *firstTouchIcon, Icon *secondTouchIcon)
{	
	int firstRow = firstTouchIcon->getRow();
	int firstCol = firstTouchIcon->getCol();
	int secondRow = secondTouchIcon->getRow();
	int secondCol = secondTouchIcon->getCol();

	auto isSameRowAccess = [&](int col)->bool
					{
						if (firstRow != secondRow)
						{
							return false;
						}
						return m_iconsMap[firstRow][col]->isAccess();
					};

	auto isSameColAccess = [&](int row)->bool
					{
						if (firstCol != secondCol)
						{
							return false;
						}
						return m_iconsMap[row][firstCol]->isAccess();
					};


	bool bRet = false;
	if (firstCol == secondCol)
	{
		bRet = isIconAccess(firstRow, secondRow, isSameColAccess);
	}
	else if (firstRow == secondRow)
	{
		bRet = isIconAccess(firstCol, secondCol, isSameRowAccess);
	}

	return bRet;
}


bool CIconsMap::isIconAccess(int first, int second, IconAccessCallback callback)
{
	int maxIndex = first>second?first:second;
	int minIndex = first<second?first:second;
	if (maxIndex == (minIndex + 1))
	{
		return true;
	}
	bool isAccess = false;
	for (int index=(minIndex+1); index<maxIndex; ++index)
	{
		isAccess = callback(index);
		if (!isAccess)
		{
			return false;
		}
	}
	return true;
}

bool CIconsMap::isOneCornerAccess(Icon *firstTouchIcon, Icon *secondTouchIcon)
{
	int firstRow = firstTouchIcon->getRow();
	int firstCol = firstTouchIcon->getCol();
	int secondRow = secondTouchIcon->getRow();
	int secondCol = secondTouchIcon->getCol();


	int cornerRows[] = {firstRow, secondRow};
	int cornerCols[] = {secondCol, firstCol};

	Icon *cornerIcon = NULL;
	int forSize = sizeof(cornerRows) / sizeof(int);
	
	for (int i=0; i<forSize; ++i)
	{
		int &cornerRow = cornerRows[i];
		int &cornerCol = cornerCols[i];
		cornerIcon = m_iconsMap[cornerRow][cornerCol];
		
		if (!cornerIcon->isAccess())
		{
			continue;
		}
		
		if (isLineAccess(firstTouchIcon, cornerIcon)
			&& isLineAccess(cornerIcon, secondTouchIcon)
			)
		{
			m_points.push_back(cornerIcon->getPosition());
			return true;
		}

	}

	return false;
}

bool CIconsMap::isTwoCornerAccess(Icon *firstTouchIcon, Icon *secondTouchIcon)
{
	int firstRow = firstTouchIcon->getRow();
	int firstCol = firstTouchIcon->getCol();


	Icon *cornerIcon = NULL;
	int col = firstCol;
	//向上
	for (int row=firstRow+1; row<m_mapRow; ++row)
	{
		
		cornerIcon = m_iconsMap[row][col];
		if (!cornerIcon->isAccess())
		{
			break;
		}
		if (isOneCornerAccess(cornerIcon, secondTouchIcon))
		{
			m_points.push_back(cornerIcon->getPosition());
			return true;
		}

	}

	//向下
	for (int row=firstRow-1; row>=0; --row)
	{
		
		cornerIcon = m_iconsMap[row][col];
		if (!cornerIcon->isAccess())
		{
			break;
		}
		if (isOneCornerAccess(cornerIcon, secondTouchIcon))
		{
			m_points.push_back(cornerIcon->getPosition());
			return true;
		}

	}

	int row = firstRow;
	//向右
	for (int col=firstCol+1; col<m_mapCol; ++col)
	{
		
		cornerIcon = m_iconsMap[row][col];
		if (!cornerIcon->isAccess())
		{
			break;
		}
		if (isOneCornerAccess(cornerIcon, secondTouchIcon))
		{
			m_points.push_back(cornerIcon->getPosition());
			return true;
		}

	}

	//向左
	for (int col=firstCol-1; col>=0; --col)
	{
		
		cornerIcon = m_iconsMap[row][col];
		if (!cornerIcon->isAccess())
		{
			break;
		}
		if (isOneCornerAccess(cornerIcon, secondTouchIcon))
		{
			m_points.push_back(cornerIcon->getPosition());
			return true;
		}

	}
	return false;
}


