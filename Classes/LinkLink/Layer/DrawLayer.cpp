#include "DrawLayer.h"
#include "Base/Base.h"
#include "../GameConst.h"

using namespace cocos2d;

std::vector<cocos2d::Point> CDrawLayer::m_points;

CDrawLayer::CDrawLayer() : m_alpha(255)
{

}


CDrawLayer::~CDrawLayer()
{

}

bool CDrawLayer::init()
{
	Layer::init();
	
	return true;
}

void CDrawLayer::initDraw(int mapRow, int mapCol)
{
	this->setContentSize(Size(ICON_GAP*mapRow, ICON_GAP*mapCol));
	this->setAnchorPoint(Point::ZERO);
	Point toPos = VisibleRect::center() - Point(ICON_GAP*mapRow/2, ICON_GAP*mapCol/2);
	this->setPosition(toPos);
	return ;
}

void CDrawLayer::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    m_customCommand.init(0);
    m_customCommand.func = CC_CALLBACK_0(CDrawLayer::onDraw, this, transform, flags);
    renderer->addCommand(&m_customCommand);

	return ;
}


void CDrawLayer::onDraw(const Mat4 &transform, uint32_t flags)
{
	if (m_points.size() < 2)
	{
		return ;
	}

    glLineWidth( 5.0f );
    DrawPrimitives::setDrawColor4B(204,232,207,m_alpha);

	Point pos = this->getPosition() + Point(ICON_GAP/2, ICON_GAP/2);
	for (unsigned int i=0; i<m_points.size()-1; ++i)
	{
		DrawPrimitives::drawLine(m_points[i]+pos, m_points[i+1]+pos);		
	}
	
	m_alpha -= 5;
	if (m_alpha < 10)
	{
		this->removeFromParent();
	}
	return ;
}

void CDrawLayer::setDrawPoints(std::vector<Point> &points)
{
	m_points.clear();
	m_points = points;
	return ;
}

