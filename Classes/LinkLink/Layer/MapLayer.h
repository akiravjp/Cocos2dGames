#ifndef _MAP_LAYER_H_
#define _MAP_LAYER_H_

#include "cocos2d.h"
#include "Base/VisibleRect.h"

#include "../Object/IconsMap.h"

class MapLayer : public cocos2d::Layer
{

public:

	MapLayer();
	~MapLayer();

public:
	virtual bool init();
	CREATE_FUNC(MapLayer);
private:
	
	void addEvents();
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);

public:
	static cocos2d::Scene* createScene();
private:
	CIconsMap *m_iconsMap;
};

#endif	//_MAP_LAYER_H_


