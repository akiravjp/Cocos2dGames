#include "MapLayer.h"
#include "DrawLayer.h"

using namespace cocos2d;

MapLayer::MapLayer() : m_iconsMap(NULL)
{
}

MapLayer::~MapLayer()
{
}

Scene* MapLayer::createScene()
{
	auto sc = Scene::create();
	auto ly = MapLayer::create();
	sc->addChild(ly);

	return sc;
}

bool MapLayer::init()
{
	Layer::init();
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("LinkLink/icon.plist");

	m_iconsMap = CIconsMap::create();
	this->addChild(m_iconsMap);

	m_iconsMap->moveToCenter();

	
	addEvents();
	return true;
}


void MapLayer::addEvents()
{
	log("addEvents !!!");
	auto touchEvt = EventListenerTouchOneByOne::create();
	touchEvt->onTouchBegan = CC_CALLBACK_2(MapLayer::onTouchBegan, this);

	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchEvt, this);
}

bool MapLayer::onTouchBegan(Touch *touch, Event *unused_event)
{
	log("Touch onTouchBegan !!!");

	Point curTouch = touch->getLocation();
	m_iconsMap->touchMap(curTouch);	

	
	//ture 后续的三个事件回调将会响应
	//false后续的三个事件回调将不再响应
	return true;
}




