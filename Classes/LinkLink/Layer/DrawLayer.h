#ifndef _DRAW_LAYER_H_
#define _DRAW_LAYER_H_

#include "cocos2d.h"
#include <vector>

class CDrawLayer : public cocos2d::Layer
{
public:
	CDrawLayer();
	~CDrawLayer();
public:
	virtual bool init();
	CREATE_FUNC(CDrawLayer);
public:	
	static void setDrawPoints(std::vector<cocos2d::Point> &points);	
	void initDraw(int mapRow, int mapCol);
private:
	virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
	void onDraw(const cocos2d::Mat4 &transform, uint32_t flags);
private:
    cocos2d::CustomCommand m_customCommand;	
	static std::vector<cocos2d::Point> m_points;
	GLubyte m_alpha;
};
#endif  //_DRAW_LAYER_H_


