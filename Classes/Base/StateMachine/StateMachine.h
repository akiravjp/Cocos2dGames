#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_
#include <map>
template <typename T> class IStateMachine
{
protected:	
	typedef std::map<int, T*> MapState;
	MapState m_stateMap;
	T *m_state;
public:
	IStateMachine():m_state(NULL){}
	virtual ~IStateMachine()
	{
		for (typename MapState::iterator iter = m_stateMap.begin(); iter != m_stateMap.end(); ++iter)
		{
			delete iter->second;
		}
	}
public:
	void insertMap(int state, T *pState)
	{
		m_stateMap[state] = pState;
		if (!m_state)
		{
			m_state = pState;
		}
	}
public: 
	void setState(int machineState)
	{
		m_state = m_stateMap[machineState];
	}
};
#endif //_STATE_MACHINE_H_

