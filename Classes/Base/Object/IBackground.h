#ifndef _IBACKGROUN_H_
#define _IBACKGROUN_H_

#include "cocos2d.h"
#include "Base/DEF.h"

class IBackground : public cocos2d::Sprite
{
public:
	IBackground(void){}
	~IBackground(void){}

public:
	bool init() override;
	virtual void setWithFile(StrMap &strs);
	virtual void moveLeft(float vx);
private:
	virtual Sprite *getBackground(const std::string &file);
	typedef std::map<int, cocos2d::Sprite *> BackgroundMap;
	BackgroundMap m_background;	
};

#define CREATE_BACKGROUND(__TYPE__) \
	CREATE_FUNC(__TYPE__); \
	static __TYPE__ *create(StrMap &strMap)\
	{\
		__TYPE__ *background = create();\
		background->setWithFile(strMap);\
		return background;\
	}

#endif //_IBACKGROUN_H_


