#ifndef _ILINK_H_
#define _ILINK_H_

#include "cocos2d.h"

template <class T>
class ILinkNode
{
public:	
	ILinkNode():m_owerWho(NULL)
	{
		initNode();
	}
	ILinkNode(T *ower):m_owerWho(ower)
	{
		initNode();
	}
public:
	void insert(class ILinkNode *inser_node)
	{
		ILinkNode *temp_node = this->m_next;

		this->m_next = inser_node;
		inser_node->m_pre = this;

		inser_node->m_next= temp_node;
		temp_node->m_pre = inser_node;
		return ;
	}
	void insert(class ILinkNode &inser_node)
	{
		ILinkNode *temp_node = this->m_next;

		this->m_next = &inser_node;
		inser_node.m_pre = this;

		inser_node.m_next= temp_node;
		temp_node->m_pre = &inser_node;
		return ;
	}
	void remove()
	{
		ILinkNode *pre = NULL;
		ILinkNode *next = NULL;
		pre = this->m_pre;
		next = this->m_next;

		pre->m_next = next;
		next->m_pre = pre;
		return ;
	}
	ILinkNode *getPre()
	{
		return m_pre;
	}
	ILinkNode *getNext()
	{
		return m_next;
	}
	T *getOwner()
	{
		return m_owerWho;
	}
private:
	void initNode()
	{
		m_next = this;
		m_pre = this;
		return ;
	}
private:
	ILinkNode *m_next;
	ILinkNode *m_pre;
	T *m_owerWho;
};

#define each_link_node(head, node) for ((node)=(head)->getNext(); (head) != (node); (node)=(node)->getNext())

#endif	//_ILINK_H_


