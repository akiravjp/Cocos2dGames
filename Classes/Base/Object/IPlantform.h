#ifndef  _IPLATFORM_H_
#define	 _IPLATFORM_H_

#include "cocos2d.h"
#include "Base/Base.h"

class IPlantform : public cocos2d::Sprite
{
public:
	IPlantform(void);
	~IPlantform(void){}

public:
	bool init() override;
	void createPlantForm(StrMap &strMap);
	float getWidth();
	void move(float vx);
private:	
	float m_width;
};

#define CREATE_PLANTFORM(__TYPE__) \
	CREATE_FUNC(__TYPE__); \
	static __TYPE__ *create(StrMap &strMap)\
	{\
		__TYPE__ *plantform = create();\
		plantform->createPlantForm(strMap);\
		return plantform;\
	}


 
#endif	 //_IPLATFORM_H_

