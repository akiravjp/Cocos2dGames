#include "Base/Object/IBackground.h"

USING_NS_CC;

bool IBackground::init()
{
	return Sprite::init();
}

void IBackground::setWithFile(StrMap &strs)
{
	for (unsigned int i=0; i<strs.size(); ++i)
	{
		m_background[i] = getBackground(strs[i]);
		this->addChild(m_background[i]);
		m_background[i]->setAnchorPoint(Point::ZERO);
	}

	for (unsigned int i=1; i<strs.size(); ++i)
	{
		cocos2d::Sprite *pre = m_background[i-1];
		cocos2d::Sprite *next = m_background[i];
		
		next->setPositionX(pre->getPositionX()+pre->getContentSize().width-1);
	}
	
	return ;
}

Sprite *IBackground::getBackground(const std::string &file)
{
	return Sprite::create(file);
}

void IBackground::moveLeft(float vx)
{
	for (unsigned int i=0; i<m_background.size(); ++i)
	{
		cocos2d::Sprite *background = m_background[i];
		background->setPositionX(background->getPositionX()-vx);
	}

	for (unsigned int i=0; i<m_background.size(); ++i)
	{
		cocos2d::Sprite *curBackground = m_background[i];
		cocos2d::Sprite *preBackground = m_background[(i+m_background.size()-1)%m_background.size()];
		
		if (curBackground->getPositionX() <= -(curBackground->getContentSize().width))
		{
			curBackground->setPositionX(preBackground->getPositionX()+preBackground->getContentSize().width-1);
		}
	}
	
	return ;
}




