#include "Base/Object/IPlantform.h"


USING_NS_CC;

IPlantform::IPlantform(void) : m_width(0)
{

}
bool IPlantform::init()
{
	if (!Sprite::init())
	{
		return false;
	}
	return true;
}


void IPlantform::createPlantForm(StrMap &strMap)
{
	float nextX = 0;
	Sprite *pSprite = NULL;
	for (unsigned int i=0; i<strMap.size(); ++i)
	{
		pSprite = Sprite::createWithSpriteFrameName(strMap[i]);
		this->addChild(pSprite);

		pSprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
		pSprite->setPositionX(nextX);

		nextX = pSprite->getPositionX() + pSprite->getContentSize().width - 1;
		m_width += pSprite->getContentSize().width - 1;
	}
	
	return;
}

float IPlantform::getWidth()
{
	return m_width;
}


void IPlantform::move(float vx)
{	
	float pox		=	this->getPositionX() - vx;
	float poy		=	this->getPositionY();
	this->setPositionX(pox);

	return ;
}


