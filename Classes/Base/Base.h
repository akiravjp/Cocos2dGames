#ifndef __BASE__H__
#define __BASE__H__
#include "cocos2d.h"
#include "../../cocos2d/cocos/editor-support/cocostudio/CocoStudio.h"
#include "cocos/ui/CocosGUI.h"

#include "Base/MultiTask/Guard.h"
#include "Base/StateMachine/StateMachine.h"
#include "Base/VisibleRect.h"
#include "Base/Object/IBackground.h"
#include "Base/Object/IPlantform.h"
#include "Base/Object/ILink.h"


#endif //__BASE__H__
